package com.mindzen.planner.security.config;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

@Component
public class WenplanLogoutHandler implements LogoutHandler {



	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		//System.out.println("logout");
		try {
			HttpSession session= request.getSession(false);
			SecurityContextHolder.clearContext();
			session= request.getSession(false);
			if(session != null) {
				session.invalidate();
			}
			for(Cookie cookie : request.getCookies()) {
				cookie.setMaxAge(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}



}
