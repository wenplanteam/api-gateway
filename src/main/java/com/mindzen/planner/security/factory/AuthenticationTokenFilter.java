package com.mindzen.planner.security.factory;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.ExpiredJwtException;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthenticationTokenFilter.
 *
 * @author Alexpandiyan Chokkan
 */
public class AuthenticationTokenFilter extends OncePerRequestFilter{

	/** The user details service. */
	private final UserDetailsService userDetailsService;

	/** The token util. */
	private final TokenUtil tokenUtil;

	/** The token header. */
	private JwtConfig jwtConfig;

	/**
	 * Instantiates a new authentication token filter.
	 *
	 * @param userDetailsService the user details service
	 * @param tokenUtil the token util
	 * @param jwtConfig the jwt config
	 */
	public AuthenticationTokenFilter(UserDetailsService userDetailsService, TokenUtil tokenUtil, JwtConfig jwtConfig) {
		this.userDetailsService = userDetailsService;
		this.tokenUtil = tokenUtil;
		this.jwtConfig = jwtConfig;
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.filter.OncePerRequestFilter#doFilterInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		final String requestHeader = request.getHeader(this.jwtConfig.getHeader());

		String username = null;
		String authToken = null;

		if(null != requestHeader && requestHeader.startsWith(jwtConfig.getPrefix()+" ")) {
			authToken = requestHeader.substring(7);

			try {
				username = tokenUtil.getUsernameFromToken(authToken);
			}
			catch(ExpiredJwtException e) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "TOKEN_EXPIRED");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				response.setContentType(MediaType.APPLICATION_JSON_VALUE);
				return;
			}
			catch (Exception e) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "TOKEN_EXPIRED");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				response.setContentType(MediaType.APPLICATION_JSON_VALUE);
				return;
			}
		}
		else {
		    SecurityContextHolder.clearContext();
 			filterChain.doFilter(request, response);
			return;
		}

		if(null != username) {
			CustomUserDetails userDetails = (CustomUserDetails) this.userDetailsService.loadUserByUsername(username.toLowerCase());

			if(tokenUtil.validateToken(authToken, userDetails)) {
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		} 

		filterChain.doFilter(request, response);
	}

}
