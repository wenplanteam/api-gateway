/**
 * 
 */
package com.mindzen.planner.security.factory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.annotation.Resource;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;

// TODO: Auto-generated Javadoc
/**
 * The Class TokenUtil.
 *
 * @author Alexpandiyan Chokkan
 */
@Component
public class TokenUtil {

	/** The claim key username. */
	final String CLAIM_KEY_USERNAME = "sub";
	
	/** The claim key created. */
	final String CLAIM_KEY_CREATED = "iat";
	
	/** The clock. */
	private Clock clock = DefaultClock.INSTANCE;
	
	/** The jwt config. */
	@Resource
	private JwtConfig jwtConfig;
	
	/**
	 * Gets the username from token.
	 *
	 * @param token the token
	 * @return the username from token
	 */
	public String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject);
	}
	
	/**
	 * Gets the issued at date from token.
	 *
	 * @param token the token
	 * @return the issued at date from token
	 */
	public Date getIssuedAtDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getIssuedAt);
	}
	
	/**
	 * Gets the expiration date from token.
	 *
	 * @param token the token
	 * @return the expiration date from token
	 */
	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}
	
	/**
	 * Gets the claim from token.
	 *
	 * @param <T> the generic type
	 * @param token the token
	 * @param claimsResolver the claims resolver
	 * @return the claim from token
	 */
	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

	/**
	 * Gets the all claims from token.
	 *
	 * @param token the token
	 * @return the all claims from token
	 */
	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser()
				.setSigningKey(jwtConfig.getSecret())
				.parseClaimsJws(token)
				.getBody();
	}
	
	/**
	 * Checks if is token expired.
	 *
	 * @param token the token
	 * @return the boolean
	 */
	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(clock.now());
	}
	
	/**
	 * Checks if is created before last password reset.
	 *
	 * @param createdDate the created date
	 * @param lastPasswordReset the last password reset
	 * @return the boolean
	 */
	private Boolean isCreatedBeforeLastPasswordReset(Date createdDate, Date lastPasswordReset) {
		return (null != lastPasswordReset && createdDate.before(lastPasswordReset));
	}

	/**
	 * Ignore token expiration.
	 *
	 * @param token the token
	 * @return the boolean
	 */
	private Boolean ignoreTokenExpiration(String token) {
		return false;
	}
	
	/**
	 * Generate token.
	 *
	 * @param userDetails the user details
	 * @return the string
	 */
	public String generateToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap<>();
		return doGenerateToken(claims, userDetails.getUsername());
	}

	/**
	 * Do generate token.
	 *
	 * @param claims the claims
	 * @param subject the subject
	 * @return the string
	 */
	public String doGenerateToken(Map<String, Object> claims, String subject) {
		final Date createdDate = clock.now();
		final Date expirationDate = calculateExpirationDate(createdDate);
		return Jwts.builder()
				.setClaims(claims)
				.setSubject(subject)
				.setIssuedAt(createdDate)
				.setExpiration(expirationDate)
				.signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret())
				.compact();
	}
	
	/**
	 * Can token be refreshed.
	 *
	 * @param token the token
	 * @param lastPasswordReset the last password reset
	 * @return the boolean
	 */
	public Boolean canTokenBeRefreshed(String token, Date lastPasswordReset) {
		final Date createdDate = getIssuedAtDateFromToken(token);
		return !isCreatedBeforeLastPasswordReset(createdDate, lastPasswordReset)
				&& (!isTokenExpired(token) || ignoreTokenExpiration(token));
	}

	/**
	 * Refresh token.
	 *
	 * @param token the token
	 * @return the string
	 */
	public String refreshToken(String token) {
		final Date createdDate = clock.now();
		final Date expirationDate = calculateExpirationDate(createdDate);
		
		final Claims claims = getAllClaimsFromToken(token);
		claims.setIssuedAt(createdDate);
		claims.setExpiration(expirationDate);
		
		return Jwts.builder()
				.setClaims(claims)
				.signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret())
				.compact();
	}
	
	/**
	 * Validate token.
	 *
	 * @param token the token
	 * @param userDetails the user details
	 * @return the boolean
	 */
	public Boolean validateToken(String token, UserDetails userDetails) {
		CustomUserDetails customUserDetails = (CustomUserDetails) userDetails;
		final String username = getUsernameFromToken(token);
		final Date createdDate = getIssuedAtDateFromToken(token);
		
		return (username.equals(customUserDetails.getUsername())
				&& !isTokenExpired(token)
				&& !isCreatedBeforeLastPasswordReset(createdDate, customUserDetails.getLastPasswordResetDate()));
	}
	
	/**
	 * Calculate expiration date.
	 *
	 * @param createdDate the created date
	 * @return the date
	 */
	private Date calculateExpirationDate(Date createdDate) {
		return new Date(createdDate.getTime() + jwtConfig.getExpiration());
	}
	
}
