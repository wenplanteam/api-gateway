/**
 * 
 */
package com.mindzen.planner.security.factory;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.mindzen.planner.dto.UserDTO;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating CustomUserDetails objects.
 *
 * @author Alexpandiyan Chokkan
 */
public class CustomUserDetailsFactory {

	/**
	 * Instantiates a new custom user details factory.
	 */
	public CustomUserDetailsFactory() {
	}
	
	/**
	 * Creates the.
	 *
	 * @param user the user
	 * @return the custom user details
	 */
	public static CustomUserDetails create(UserDTO user) {
		return new CustomUserDetails(
				user.getId(),
				user.getEmail(),
				user.getPassword(),
				user.getFirstName(),
				user.getLastName(),
				user.isActive(),
				user.isEmailVerified(),
				user.getLastPasswordResetDate(),
				user.isAccountExpired(),
				user.isCredentialsExpired(),
				mapToGrantedAuthorities(user.getRoleName()));
	}

	/**
	 * Map to granted authorities.
	 *
	 * @param roleName the role name
	 * @return the sets the
	 */
	private static Set<GrantedAuthority> mapToGrantedAuthorities(String roleName) {
		Set<GrantedAuthority> authorities = new HashSet<>();
		authorities.add(new SimpleGrantedAuthority(roleName));
		return authorities;
	}
	
}
