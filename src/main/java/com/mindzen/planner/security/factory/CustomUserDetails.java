/**
 * 
 */
package com.mindzen.planner.security.factory;

import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomUserDetails.
 *
 * @author Alexpandiyan Chokkan
 */
public class CustomUserDetails implements UserDetails {
	
	/** The id. */
	private Long id;
	
	/** The username. */
	private String username;
	
	/** The password. */
	private String password;
	
	/** The first name. */
	private String firstName;
	
	/** The last name. */
	private String lastName;
	
	/** The enabled. */
	private boolean enabled;
	
	/** The last password reset date. */
	private Date lastPasswordResetDate;
	
	/** The is account non expired. */
	private boolean isAccountNonExpired;
	
	/** The is email verified. */
	private boolean isEmailVerified;
	
	/** The is credentials non expired. */
	private boolean isCredentialsNonExpired;
	
	/** The authorities. */
	private Collection<? extends GrantedAuthority> authorities;
	
	/**
	 * Instantiates a new custom user details.
	 *
	 * @param id the id
	 * @param email the email
	 * @param password the password
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param isActive the is active
	 * @param isEmailVerified the is email verified
	 * @param lastPasswordResetDate the last password reset date
	 * @param isAccountExpired the is account expired
	 * @param isCredentialsExpired the is credentials expired
	 * @param authorities the authorities
	 */
	public CustomUserDetails(
			Long id,
			String email,
			String password,
			String firstName,
			String lastName,
			boolean isActive,
			boolean isEmailVerified,
			Date lastPasswordResetDate,
			boolean isAccountExpired,
			boolean isCredentialsExpired,
			Collection<? extends GrantedAuthority> authorities
	) {
		this.id = id;
		this.username = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.enabled = isActive;
		this.isEmailVerified = isEmailVerified;
		this.lastPasswordResetDate = lastPasswordResetDate;
		this.isAccountNonExpired = !isAccountExpired;
		this.isCredentialsNonExpired = !isCredentialsExpired;
		this.authorities = authorities;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#getAuthorities()
	 */
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#getUsername()
	 */
	public String getUsername() {
		return username;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#isAccountNonExpired()
	 */
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return isAccountNonExpired;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#isAccountNonLocked()
	 */
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return isEmailVerified;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#isCredentialsNonExpired()
	 */
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return isCredentialsNonExpired;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#isEnabled()
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#getPassword()
	 */
	@JsonIgnore
	public String getPassword() {
		return password;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Gets the last password reset date.
	 *
	 * @return the last password reset date
	 */
	@JsonIgnore
	public Date getLastPasswordResetDate() {
		return lastPasswordResetDate;
	}

}
