/**
 * 
 */
package com.mindzen.planner.security.factory;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthenticationResponse.
 *
 * @author Alexpandiyan Chokkan
 */
public class AuthenticationResponse {

	/** The token. */
	private final String token;
	
	/**
	 * Instantiates a new authentication response.
	 *
	 * @param token the token
	 */
	public AuthenticationResponse(String token) {
		this.token = token;
	}
	
	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
}
