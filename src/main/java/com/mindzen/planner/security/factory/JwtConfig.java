/**
 * 
 */
package com.mindzen.planner.security.factory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

// TODO: Auto-generated Javadoc
/**
 * The Class JwtConfig.
 *
 * @author Alexpandiyan Chokkan
 * 
 * 25-Sep-2018
 */
@Configuration
public class JwtConfig {

	/** The uri. */
	@Value("${spring.security.jwt.route.auth}")
	private String uri;
	
	/** The header. */
	@Value("${spring.security.jwt.header}")
	private String header;
	
	/** The prefix. */
	@Value("${spring.security.jwt.prefix}")
	private String prefix;
	
	/** The expiration. */
	@Value("#{T(java.lang.Integer).parseInt(${spring.security.jwt.expiration})}")
	private int expiration;
	
	/** The secret. */
	@Value("${spring.security.jwt.secret}")
	private String secret;

	/**
	 * Gets the uri.
	 *
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * Sets the uri.
	 *
	 * @param uri the new uri
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * Gets the header.
	 *
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * Sets the header.
	 *
	 * @param header the new header
	 */
	public void setHeader(String header) {
		this.header = header;
	}

	/**
	 * Gets the prefix.
	 *
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * Sets the prefix.
	 *
	 * @param prefix the new prefix
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * Gets the expiration.
	 *
	 * @return the expiration
	 */
	public int getExpiration() {
		return expiration;
	}

	/**
	 * Sets the expiration.
	 *
	 * @param expiration the new expiration
	 */
	public void setExpiration(int expiration) {
		this.expiration = expiration;
	}

	/**
	 * Gets the secret.
	 *
	 * @return the secret
	 */
	public String getSecret() {
		return secret;
	}

	/**
	 * Sets the secret.
	 *
	 * @param secret the new secret
	 */
	public void setSecret(String secret) {
		this.secret = secret;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "JwtConfig [uri=" + uri + ", header=" + header + ", prefix=" + prefix + ", expiration=" + expiration
				+ ", secret=" + secret + "]";
	}
	
}
