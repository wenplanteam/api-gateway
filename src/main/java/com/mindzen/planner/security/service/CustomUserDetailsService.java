/**
 * 
 */
package com.mindzen.planner.security.service;

import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.feign.UserLicenseFeignClient;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.security.factory.CustomUserDetailsFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomUserDetailsService.
 *
 * @author Alexpandiyan Chokkan
 */
@Service("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	/** The user license feign client. */
	private UserLicenseFeignClient userLicenseFeignClient;
	
	/** The object mapper. */
	private ObjectMapper objectMapper;
	
	/**
	 * Instantiates a new custom user details service.
	 *
	 * @param userLicenseFeignClient the user license feign client
	 * @param objectMapper the object mapper
	 */
	public CustomUserDetailsService(UserLicenseFeignClient userLicenseFeignClient, ObjectMapper objectMapper) {
		this.userLicenseFeignClient = userLicenseFeignClient;
		this.objectMapper = objectMapper;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		MSAResponse response = userLicenseFeignClient.findByEmail(userName.toLowerCase());
		
		UserDTO userDTO = null;
		if(response.isSuccess()) {
			userDTO = objectMapper.convertValue(response.getPayload(), UserDTO.class);
		}
		if(null == userDTO)
			throw new InternalAuthenticationServiceException("Email Not Found");
		else if(!userDTO.isEmailVerified())
			throw new InternalAuthenticationServiceException("Email Not Verified");

		else
			return CustomUserDetailsFactory.create(userDTO);
	}

}
