package com.mindzen.planner.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mindzen.planner.dto.AuthenticationRequest;
import com.mindzen.planner.dto.CompanyDTO;
import com.mindzen.planner.dto.LicenseDistributionDTO;
import com.mindzen.planner.dto.PlanDTO;
import com.mindzen.planner.dto.StripeDataDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.dto.UserPlanMasterDTO;
import com.mindzen.planner.service.UserLicenseService;

import io.swagger.annotations.Api;

// TODO: Auto-generated Javadoc
/**
 * The Class UserLicenseCompositionController.
 */
@Api(tags = "User License Composition Controller")
@RequestMapping("/ulc")
@RestController()
public class UserLicenseCompositionController {

	/** The user license service. */
	@Resource
	UserLicenseService userLicenseService;

	/** The password encoder. */
	@Resource
	PasswordEncoder passwordEncoder;

	/**
	 * Load users.
	 *
	 * @param id the id
	 * @param createdBy the created by
	 * @param projectId the project id
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param created the created
	 * @return the response entity
	 */
	@GetMapping("user")
	public ResponseEntity<?> loadUsers(@RequestParam(required = false, value = "id") Long id,
			@RequestParam(required = false ,value = "createdBy") Boolean createdBy,
			@RequestParam(required = false ,value = "projectId") Long projectId,
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = true, value = "created") Long created) {
		return userLicenseService.loadUsers(id, createdBy, projectId, pageNo, pageSize, created);
	}

	/**
	 * Find by email.
	 *
	 * @param email the email
	 * @return the response entity
	 */
	@GetMapping("/findUserByEmail")
	public ResponseEntity<?> findByEmail(@RequestParam("email") String email) {
		return userLicenseService.findByEmail(email);
	}

	/**
	 * Creates the user by basic info.
	 *
	 * @param userDTOs the user DT os
	 * @return the response entity
	 */
	@PostMapping("/createUserByBasicInfo")
	public ResponseEntity<?> createUserByBasicInfo(@RequestBody List<UserDTO> userDTOs) {
		return userLicenseService.createUserByBasicInfo(userDTOs);
	}

	/**
	 * Update user.
	 *
	 * @param userDTO the user DTO
	 * @return the response entity
	 */
	@PutMapping("user")
	public ResponseEntity<?> updateUser(@RequestBody UserDTO userDTO) {
		return userLicenseService.updateUser(userDTO);
	}

	/**
	 * Delete user.
	 *
	 * @param userIds the user ids
	 * @return the response entity
	 */
	@DeleteMapping("/user")
	public ResponseEntity<?> deleteUser(@RequestBody List<Long> userIds) {
		return userLicenseService.deleteUser(userIds);
	}

	/**
	 * List ofusers.
	 *
	 * @param userDTOs the user DT os
	 * @return the response entity
	 */
	@PostMapping("/projectUsersList")
	public ResponseEntity<?> listOfusers(@RequestBody List<UserDTO> userDTOs) {
		return userLicenseService.listOfUsers(userDTOs);
	}

	/**
	 * Find project user by project id.
	 *
	 * @param projectId the project id
	 * @return the response entity
	 */
	@GetMapping("/projectUsers")
	public ResponseEntity<?>  findProjectUserByProjectId(@RequestParam(required = true,value = "projectId") Long projectId ) {
		return userLicenseService.findProjectUserByProjectId(projectId);
	}

	/**
	 * Delete user.
	 *
	 * @param email the email
	 * @return the response entity
	 */
	@DeleteMapping("/delete")
	public ResponseEntity<?> deleteUser(@RequestParam(required = true,value = "email") String email) {
		return userLicenseService.deleteUserByEmail(email);
	}

	/**
	 * Change password.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param AuthenticationRequest the authentication request
	 * @return the response entity
	 */ 
	@PostMapping("/changePassword")
	public ResponseEntity<?> changePassword(HttpServletRequest httpServletRequest, @RequestBody AuthenticationRequest AuthenticationRequest) {
		return userLicenseService.changePassword(httpServletRequest, AuthenticationRequest);
	}

	/**
	 * Find company by user id.
	 *
	 * @return the response entity
	 */
	@GetMapping("/findCompanyByUserId")
	public ResponseEntity<?> findCompanyByUserId() {
		return userLicenseService.findCompanyByUserId();
	}

	/**
	 * Get companyList / company based on company id .
	 *
	 * @param id the id
	 * @param createdBy the created by
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the response entity
	 */
	@GetMapping("/company")
	public ResponseEntity<?> companyList(@RequestParam(required = false ,value = "id") Long id,
			@RequestParam(required = false ,value = "createdBy") Boolean createdBy,
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize){
		return userLicenseService.companyList(id,createdBy,pageNo,pageSize);
	}

	/**
	 * Insert company informations.
	 *
	 * @param companyDTO the company DTO
	 * @return the response entity
	 */
	@PostMapping("/company")
	public ResponseEntity<?> insertCompany(@RequestBody CompanyDTO companyDTO){
		return userLicenseService.insertCompany(companyDTO);
	}

	/**
	 * update company informations.
	 *
	 * @param companyDTO the company DTO
	 * @return the response entity
	 */
	@PutMapping("/company")
	public ResponseEntity<?> updateCompany(@RequestBody CompanyDTO companyDTO){
		return userLicenseService.updateCompany(companyDTO);
	}

	/**
	 * In activate company.
	 *
	 * @param companyId the company id
	 * @return the response entity
	 */
	@DeleteMapping("/company")
	public ResponseEntity<?> inActivateCompany(@RequestBody List<Long> companyId) {
		return userLicenseService.inActivateCompany(companyId);
	}

	/*      
	 *  
	 *  License services
	 *  
	 */
	/**
	 * Load plans.
	 *
	 * @param paid the paid
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param id the id
	 * @return the response entity
	 */
	//@PreAuthorize("#userId == 551")
	@GetMapping("/plan")
	public ResponseEntity<?> loadPlans(@RequestParam(required = false ,value = "paid") boolean paid,
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = false ,value = "id") Long id) {
		return userLicenseService.getPlans(paid, pageNo, pageSize, id);
	}

	/**
	 * Creates the plan.
	 *
	 * @param planDTO the plan DTO
	 * @return the response entity
	 */
	@PostMapping("/plan")
	public ResponseEntity<?> createPlan(@RequestBody PlanDTO planDTO) {
		return userLicenseService.createPlan(planDTO);
	}

	/**
	 * Update plan.
	 *
	 * @param planDTO the plan DTO
	 * @return the response entity
	 */
	@PutMapping("/plan")
	public ResponseEntity<?> updatePlan(@RequestBody PlanDTO planDTO) {
		return userLicenseService.updatePlan(planDTO);
	}

	/**
	 * Delete plan.
	 *
	 * @param planId the plan id
	 * @return the response entity
	 */
	@DeleteMapping("/plan")
	public ResponseEntity<?> deletePlan(@RequestBody List<Long> planId) {
		return userLicenseService.deletePlan(planId);
	}

	/**
	 * User licenses.
	 *
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param status the status
	 * @return the response entity
	 */
	@GetMapping("/licenses")
	public ResponseEntity<?> userLicenses(
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = false, value = "status") String status)  {
		return userLicenseService.getUserLicenses(pageNo, pageSize, status);
	}

	/**
	 * Buy license.
	 *
	 * @param userPlanMasterDTO the user plan master DTO
	 * @return the response entity
	 */
	@PostMapping("/buyLicense")
	public ResponseEntity<?> buyLicense(@RequestBody UserPlanMasterDTO userPlanMasterDTO) {
		return userLicenseService.buyLicense(userPlanMasterDTO);
	}

	/**
	 * get license count based on which user logged in.
	 *
	 * @return the response entity
	 */
	@GetMapping("licenseCount")
	public ResponseEntity<?> userLicenseCount() {
		return userLicenseService.userLicenseCount();
	}

	/**
	 * Provide license.
	 *
	 * @param licenseDistributionDTO provide license call for user MSA
	 * @return the response entity
	 */
	@PostMapping("/provideLicense")
	public ResponseEntity<?> provideLicense(@RequestBody LicenseDistributionDTO licenseDistributionDTO) {
		return userLicenseService.provideLicense(licenseDistributionDTO);
	}

	/**
	 * Revoke license.
	 *
	 * @param licenseDistributionDTO revoke license call for user MSA
	 * @return the response entity
	 */
	@PostMapping("/revokeLicense")
	public ResponseEntity<?> revokeLicense(@RequestBody LicenseDistributionDTO licenseDistributionDTO) {
		return userLicenseService.revokeLicense(licenseDistributionDTO);
	}

	/**
	 * Basic info user create.
	 *
	 * @param userDTO the user DTO
	 * @return the response entity
	 */
	@PostMapping("/createUserByUserInfo")
	public ResponseEntity<?>  basicInfoUserCreate(@RequestBody UserDTO userDTO){
		return userLicenseService.basicInfoUserCreate(userDTO);
	}

	/**
	 * Find users by email.
	 *
	 * @param email the email
	 * @return the response entity
	 */
	@GetMapping("/findUsersByEmail")
	public ResponseEntity<?>  findUsersByEmail(@RequestParam(required=true, value = "email") String email) {
		return userLicenseService.findUsersByEmail(email);
	}

	/**
	 * Gets the license response.
	 *
	 * @param stripeData the stripe data
	 * @return the license response
	 */
	@PostMapping("/charge")
	public ResponseEntity<?>  getLicenseResponse(@RequestBody StripeDataDTO stripeData) {
		return userLicenseService.getLicenseResponse(stripeData);
	}

	/**
	 * Find expirydate.
	 *
	 * @param billingCycle the billing cycle
	 * @return the response entity
	 */
	@GetMapping("/expirydate")
	public ResponseEntity<?> findExpirydate(@RequestParam(required = false, value = "billingCycle") String billingCycle){
		return userLicenseService.findExpiryDate(billingCycle);
	}

	/**
	 * Gets the all user informations.
	 *
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the all user informations
	 */
	@GetMapping("/userDetails")
	public ResponseEntity<?> getAllUserInformations(
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize) {
		return userLicenseService.getAllUserInformations(pageNo,pageSize);
	}
}
