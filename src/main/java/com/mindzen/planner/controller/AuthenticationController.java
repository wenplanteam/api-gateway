package com.mindzen.planner.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.mindzen.planner.dto.AuthenticationRequest;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.service.UserLicenseService;

import io.swagger.annotations.Api;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthenticationController.
 */
@Api(tags ="Authentication Controller")
@RequestMapping("/auth")
@RestController()
public class AuthenticationController {

	/** The user license service. */
	@Resource
	private UserLicenseService userLicenseService;
	
	/**
	 * Creates the user.
	 *
	 * @param userDTO the user DTO
	 * @return the response entity
	 */
	@PostMapping("/user")
	public ResponseEntity<?> createUser(@Valid @RequestBody UserDTO userDTO) {
		return userLicenseService.createUser(userDTO);
	}

	/**
	 * Gets the authenticated user.
	 *
	 * @param request the request
	 * @return the authenticated user
	 */
	@PreAuthorize("hasRole(ADMIN)")
	@GetMapping("/user")
	public ResponseEntity<?> getAuthenticatedUser(HttpServletRequest request) {
		return userLicenseService.getAuthenticatedUser(request);
	}

	/**
	 * Creates the authentication token.
	 *
	 * @param authenticationRequest the authentication request
	 * @return the response entity
	 */
	@PostMapping("/login")
	public ResponseEntity<?> createAuthenticationToken(@Valid @RequestBody AuthenticationRequest authenticationRequest) {
		return userLicenseService.createAuthenticationToken(authenticationRequest);
	}

	/**
	 * Refresh and get authentication token.
	 *
	 * @param request the request
	 * @return the response entity
	 */
	@GetMapping("/refresh")
	public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
		return userLicenseService.refreshAndGetAuthenticationToken(request);
	}

	/**
	 * Verify email.
	 *
	 * @param email the email
	 * @param source the source
	 * @return the redirect view
	 */
	@GetMapping("/verifyEmail")
	public RedirectView verifyEmail(@RequestParam(required = true, value = "email") String email,
			@RequestParam(required = true ,value = "source") String source) {
		return new RedirectView(userLicenseService.verifyEmail(email, source));
	}

	/**
	 * Decode.
	 *
	 * @param data the data
	 * @return the response entity
	 */
	@GetMapping("/decode")
	public ResponseEntity<?> decode(@RequestParam(required = true, value = "data") String data) {
		return userLicenseService.decode(data);
	}
	
	/**
	 * Forgot password.
	 *
	 * @param authenticationRequest the authentication request
	 * @return the response entity
	 */
	@PostMapping("/forgotPassword")
	public ResponseEntity<?> forgotPassword(@RequestBody AuthenticationRequest authenticationRequest){
		return userLicenseService.forgotPassword(authenticationRequest);
	}
	
	/**
	 * Forgot password.
	 *
	 * @param email the email
	 * @param source the source
	 * @return the redirect view
	 */
	@GetMapping("/forgotPassword")
	public RedirectView forgotPassword(@RequestParam(required = true, value = "email") String email,
			@RequestParam(required = true ,value = "source") String source){
		return new RedirectView(userLicenseService.forgotPassword(email, source));
	}
	
	/**
	 * Success logout.
	 *
	 * @param request the request
	 * @param response the response
	 * @param authentication the authentication
	 * @return the response entity
	 */
	@GetMapping("/logout")
	public ResponseEntity<?> successLogout(HttpServletRequest request, HttpServletResponse response, Authentication authentication){
		return userLicenseService.successLogout(request,response,authentication);
	}
	
}
