package com.mindzen.planner.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mindzen.planner.dto.ImportHistoryDTO;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.LookaheadListDTO;
import com.mindzen.planner.dto.ProgressCompletionRequestDTO;
import com.mindzen.planner.dto.ProjectCompanyDTO;
import com.mindzen.planner.dto.ProjectDTO;
import com.mindzen.planner.dto.ProjectUserDTO;
import com.mindzen.planner.service.ProjectManagementService;

import io.swagger.annotations.Api;

/**
 * The Class ProjectManagementCompositionController.
 */
@Api(tags = "Project Management Composition Controller")
@RequestMapping("pmc")
@RestController()
public class ProjectManagementCompositionController {

	/** The project management service. */
	@Resource
	ProjectManagementService projectManagementService;

	/**
	 * Post project.
	 *
	 * @param projectDTO the project DTO
	 * @return the response entity
	 */
	@PostMapping("/project")
	public ResponseEntity<?> postProject(@Valid @RequestBody ProjectDTO projectDTO) {
		return projectManagementService.createProject(projectDTO);
	}

	/**
	 * Share project to user.
	 *
	 * @param projectUserDTO the project user DTO
	 * @return the response entity
	 */
	@PostMapping("/shareProject")
	public ResponseEntity<?> shareProjectToUser(@RequestBody ProjectUserDTO projectUserDTO) {
		return projectManagementService.shareProject(projectUserDTO);
	}

	/**
	 * Update project.
	 *
	 * @param projectDTO the project DTO
	 * @return the response entity
	 */
	@PutMapping("/project")
	public ResponseEntity<?> updateProject(@Valid @RequestBody ProjectDTO projectDTO) {
		return projectManagementService.updateProject(projectDTO);
	}

	/**
	 * Delete project.
	 *
	 * @param projectId the project id
	 * @return the response entity
	 */
	@DeleteMapping("/project")
	public ResponseEntity<?> deleteProject(@Valid @RequestBody Long[] projectId) {
		return projectManagementService.deleteProject(projectId);
	}

	/**
	 * Load all projects.
	 *
	 * @param pageNo        the page no
	 * @param pageSize      the page size
	 * @param projectStatus the project status
	 * @param searchTerm    the search term
	 * @param projectId     the project id
	 * @param created       the created
	 * @return the response entity
	 */
	@GetMapping("/project")
	public ResponseEntity<?> loadAllProjects(@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = false, value = "projectStatus") Boolean projectStatus,
			@RequestParam(required = false, value = "searchTerm") String searchTerm,
			@RequestParam(required = false, value = "projectId") Long projectId,
			@RequestParam(required = true, value = "created") Long created) {
		return projectManagementService.loadAllProjects(pageNo, pageSize, projectStatus, searchTerm, projectId,
				created);
	}

	/**
	 * Find by project id.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the response entity
	 */
	/*
	 * @GetMapping("/project") public ResponseEntity<?> findByProjectId() { return
	 * projectManagementService.findByProjectId(projectId); }
	 */

	/**
	 * Creates the lookahead.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the response entity
	 */
	@PostMapping("/lookahead")
	public ResponseEntity<?> createLookahead(@RequestBody LookaheadDTO lookaheadDTO) {
		return projectManagementService.createLookahead(lookaheadDTO);
	}

	/**
	 * List lookahead.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the response entity
	 */
	@PostMapping("/lookaheads")
	public ResponseEntity<?> listLookahead(@RequestBody LookaheadListDTO lookaheadListDTO) {
		return projectManagementService.listLookahead(lookaheadListDTO);
	}

	/**
	 * Find project company by project id.
	 *
	 * @param projectId the project id
	 * @return the response entity
	 */
	@GetMapping("projectCompany")
	public ResponseEntity<?> findProjectCompanyByProjectId(
			@RequestParam(required = true, value = "projectId") Long projectId) {
		return projectManagementService.findProjectCompanyByProjectId(projectId);
	}

	/**
	 * Insert companies using project id.
	 *
	 * @param projectCompanyDTO the project company DTO
	 * @return the response entity
	 */
	@PostMapping("projectCompany")
	public ResponseEntity<?> insertProjectCompanyByProjectId(@RequestBody ProjectCompanyDTO projectCompanyDTO) {
		return projectManagementService.insertProjectCompanyByProjectId(projectCompanyDTO);
	}

	/**
	 * Find by project for plan sharing.
	 *
	 * @param projectId the project id
	 * @return the response entity
	 */
	@GetMapping("/projectPlanShare")
	public ResponseEntity<?> findByProjectForPlanSharing(
			@RequestParam(required = true, value = "projectId") Long projectId) {
		return projectManagementService.findByProjectForPlanSharing(projectId);
	}

	/**
	 * Lookahead info.
	 *
	 * @param projectId the project id
	 * @return the response entity
	 */
	@GetMapping("/lookaheadDateInfo")
	public ResponseEntity<?> lookaheadInfo(@RequestParam(required = true) Long projectId) {
		return projectManagementService.lookaheadInfo(projectId);
	}

	/**
	 * Update lookahead.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the response entity
	 */
	@PutMapping("/lookahead")
	public ResponseEntity<?> updateLookahead(@RequestBody LookaheadDTO lookaheadDTO) {
		return projectManagementService.updateLookahead(lookaheadDTO);
	}

	/**
	 * Lookahead by id.
	 *
	 * @param taskId    the task id
	 * @param subTaskId the sub task id
	 * @return the response entity
	 */
	@DeleteMapping("lookaheadById")
	public ResponseEntity<?> lookaheadById(@RequestParam(required = false, value = "taskId") Long taskId,
			@RequestParam(required = false, value = "subTaskId") Long subTaskId) {
		return projectManagementService.lookaheadById(taskId, subTaskId);
	}

	/**
	 * Lookahead PDF report.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the response entity
	 */
	@PostMapping("/lookaheadPDFReport")
	public ResponseEntity<?> lookaheadPDFReport(@RequestBody LookaheadListDTO lookaheadListDTO) {
		return projectManagementService.lookaheadPDFReport(lookaheadListDTO);
	}

	/**
	 * Fetch week start date and leave day of the week.
	 *
	 * @param projectId the project id
	 * @return the project info
	 */
	@GetMapping("/projectInfo")
	public ResponseEntity<?> getProjectInfo(@RequestParam(required = true, value = "projectId") Long projectId) {
		return projectManagementService.getProjectInfo(projectId);
	}

	/**
	 * Insert week start date and leave day of the week.
	 *
	 * @param projectDTO the project DTO
	 * @return the response entity
	 */
	@PostMapping("/projectInfo")
	public ResponseEntity<?> insertProjectInfo(@RequestBody ProjectDTO projectDTO) {
		return projectManagementService.insertProjectInfo(projectDTO);
	}

	/**
	 * Insert trade.
	 *
	 * @param projectDTO the project DTO
	 * @return the response entity
	 */
	@PostMapping("/trade")
	public ResponseEntity<?> insertTrade(@RequestBody ProjectDTO projectDTO) {
		return projectManagementService.insertTrade(projectDTO);
	}

	/**
	 * Gets the trade.
	 *
	 * @param projectId the project id
	 * @return the trade
	 */
	@GetMapping("/trade")
	public ResponseEntity<?> getTrade(@RequestParam(required = true, value = "projectId") Long projectId) {
		return projectManagementService.getTrade(projectId);
	}

	/**
	 * Insert spec division.
	 *
	 * @param projectDTO the project DTO
	 * @return the response entity
	 * @author MRG
	 */
	@PostMapping("/specDivision")
	public ResponseEntity<?> insertSpecDivision(@RequestBody ProjectDTO projectDTO) {
		return projectManagementService.insertSpecDivision(projectDTO);
	}

	/**
	 * Gets the trade.
	 *
	 * @param projectId the project id
	 * @return the trade
	 * @author MRG
	 */
	@GetMapping("/specDivision")
	public ResponseEntity<?> getSpecDivision(@RequestParam(required = true, value = "projectId") Long projectId) {
		return projectManagementService.getSpecDivision(projectId);
	}

	/**
	 * Download PDF.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @param request          the request
	 * @param response         the response
	 * @return the response entity
	 */
	@PostMapping("/downloadPDF")
	public ResponseEntity<?> downloadPDF(@RequestBody LookaheadListDTO lookaheadListDTO, HttpServletRequest request,
			HttpServletResponse response) {
		return projectManagementService.downloadPDF(lookaheadListDTO, request, response);
	}

	/**
	 * Gets the GC by user.
	 *
	 * @return the GC by user
	 */
	@GetMapping("/gcByUser")
	public ResponseEntity<?> getGCByUser() {
		return projectManagementService.getGCByUser();
	}

	/**
	 * Gets the task summary mail.
	 *
	 * @param projectId the project id
	 * @return the task summary mail
	 * @author MRG
	 */
	@GetMapping("/taskSummaryMail/{projectId}")
	public ResponseEntity<?> getTaskSummaryMail(@PathVariable(required = true, value = "projectId") Long projectId) {
		return projectManagementService.getTaskSummaryMail(projectId);
	}

	/**
	 * Upload lookahead excel.
	 *
	 * @param fileName         the file name
	 * @param projectId        the project id
	 * @param projectCompanyId the project company id
	 * @return the response entity
	 */
	@PostMapping("/uploadLookaheadExcelFileName")
	public ResponseEntity<?> uploadLookaheadExcel(@RequestParam String fileName, @RequestParam Long projectId,
			@RequestParam Long projectCompanyId) {
		return projectManagementService.uploadExcelToLookahead(null, fileName, projectId, projectCompanyId);
	}

	/**
	 * Gets the progress completion by project id.
	 *
	 * @param projectCompletionRequestDTO the project completion request DTO
	 * @return the progress completion by project id
	 * @author MRG
	 */
	@PostMapping("/progressCompletionByProjectId")
	public ResponseEntity<?> getProgressCompletionByProjectId(
			@Valid @RequestBody ProgressCompletionRequestDTO projectCompletionRequestDTO) {
		return projectManagementService.getProgressCompletionByProjectId(projectCompletionRequestDTO);
	}

	@PostMapping("/downloadPercentageCompletePDF")
	public ResponseEntity<?> downloadPercentageCompletePDF(
			@RequestBody ProgressCompletionRequestDTO progressCompletionRequestDTO) {
		return projectManagementService.downloadPercentageCompletePDF(progressCompletionRequestDTO);
	}

	/**
	 * This method written for import excel and now added mpp file import also
	 */
	@PostMapping("/importExcel")
	public ResponseEntity<?> importExcel(@RequestBody(required = true) MultipartFile fileName,
			@RequestParam Long projectId, @RequestParam Long projectCompanyId, HttpServletRequest request) {
		String extension = com.google.common.io.Files.getFileExtension(fileName.getOriginalFilename());
		if (extension.equalsIgnoreCase("mpp"))
			return projectManagementService.importmppFile(fileName, projectId, projectCompanyId, request);
		else if (extension.equalsIgnoreCase("xer"))
			return projectManagementService.importXerFile(fileName, projectId, projectCompanyId, request);
		else
			return projectManagementService.importExcel(fileName, projectId, projectCompanyId, request);
	}

	@GetMapping("/formatExcel")
	public ResponseEntity<?> formatExcel() {
		return projectManagementService.formatExcel();
	}

	@PutMapping("/updateMppFile")
	public ResponseEntity<?> updateMppFile(@RequestBody(required = true) MultipartFile fileName,
			@RequestParam Long projectId, @RequestParam Long projectCompanyId, @RequestParam Long importHistoryId,
			HttpServletRequest request) {

		// String extension =
		// com.google.common.io.Files.getFileExtension(fileName.getOriginalFilename());

		// if(extension.equalsIgnoreCase("mpp"))
		return projectManagementService.updateMppFile(fileName, projectId, projectCompanyId, importHistoryId, request);
		// else if(extension.equalsIgnoreCase("xer"))
		// return
		// projectManagementService.importXerFile(fileName,projectId,projectCompanyId,request);
		// else
		// return new ResponseEntity<>("File not supported", HttpStatus.OK);

	}

	@PutMapping("/clearSchedule")
	public ResponseEntity<?> clearSchedule(@RequestParam Long projectId, @RequestParam Long projectCompanyId,
			@RequestParam Long importHistoryId) {
		return projectManagementService.clearSchedule(projectId, projectCompanyId, importHistoryId);

	}

	@GetMapping("/importHistory")
	public ResponseEntity<?> importHistory(@RequestParam Long projectId, @RequestParam Integer pageNo,@RequestParam Integer pageSize) {
		return projectManagementService.importHistory(projectId,pageNo,pageSize);
	}

}