package com.mindzen.planner.controller;

import javax.annotation.Resource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindzen.planner.service.UtilityService;

import io.swagger.annotations.Api;

// TODO: Auto-generated Javadoc
/**
 * The Class UtilityCompositionController.
 */
@Api(tags ="Utility composition Controller")
@RequestMapping("util")
@RestController()
public class UtilityCompositionController {

	/** The utility service. */
	@Resource
	private UtilityService utilityService;

	/**
	 * Load masters.
	 *
	 * @return the response entity
	 */
	@GetMapping("/masters")
	public ResponseEntity<?> loadMasters() {
		return utilityService.loadMasters();
	}
	
	/**
	 * Load time zones.
	 *
	 * @return the response entity
	 */
	@GetMapping("/timeZones")
	public ResponseEntity<?> loadTimeZones() {
		return utilityService.loadTimeZones();
	}

}
