/**
 * 
 */
package com.mindzen.planner.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.LookaheadListDTO;
import com.mindzen.planner.dto.ProgressCompletionRequestDTO;
import com.mindzen.planner.dto.ProjectCompanyDTO;
import com.mindzen.planner.dto.ProjectDTO;
import com.mindzen.planner.dto.ProjectUserDTO;

/**
 * The Interface ProjectManagementService.
 *
 * @author Alexpandiyan Chokkan
 * 
 * 25-Sep-2018
 */
public interface ProjectManagementService {

	/**
	 * Share project.
	 *
	 * @param projectUserDTO the project user DTO
	 * @return the response entity
	 */
	ResponseEntity<?> shareProject(ProjectUserDTO projectUserDTO);

	/**
	 * Creates the project.
	 *
	 * @param projectDTO the project DTO
	 * @return the response entity
	 */
	ResponseEntity<?> createProject(ProjectDTO projectDTO);

	/**
	 * Update project.
	 *
	 * @param projectDTO the project DTO
	 * @return the response entity
	 */
	ResponseEntity<?> updateProject(ProjectDTO projectDTO);

	/**
	 * Find by project id.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the response entity
	 */
	//ResponseEntity<?>  findByProjectId(Long projectId);

	/**
	 * Creates the lookahead.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the response entity
	 */
	ResponseEntity<?> createLookahead(LookaheadDTO lookaheadDTO);

	/**
	 * List lookahead.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @param pageSize 
	 * @param pageNo 
	 * @return the response entity
	 */
	ResponseEntity<?> listLookahead(LookaheadListDTO lookaheadListDTO);
	
	/**
	 * Delete project.
	 *
	 * @param projectId the project id
	 * @return the response entity
	 */
	ResponseEntity<?> deleteProject(Long[] projectId);

	/**
	 * Find project company by project id.
	 *
	 * @param projectId the project id
	 * @return the response entity
	 */
	ResponseEntity<?> findProjectCompanyByProjectId(Long projectId);

	/**
	 * Load all projects.
	 *
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param status the status
	 * @param searchTerm the search term
	 * @param projectId the project id
	 * @param created the created
	 * @return the response entity
	 */
	ResponseEntity<?> loadAllProjects(Integer pageNo, Integer pageSize, Boolean status, String searchTerm, Long projectId, Long created);

	/**
	 * Find by project for plan sharing.
	 *
	 * @param projectId the project id
	 * @return the response entity
	 */
	ResponseEntity<?> findByProjectForPlanSharing(Long projectId);

	
	/**
	 * Lookahead info.
	 *
	 * @param projectId the project id
	 * @return calculate current date  and lookahead date
	 */
	ResponseEntity<?> lookaheadInfo(Long projectId);

	/**
	 * Update lookahead.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return update lookahead
	 */
	ResponseEntity<?> updateLookahead(LookaheadDTO lookaheadDTO);

	/**
	 * Lookahead by id.
	 *
	 * @param taskId the task id
	 * @param subTaskId the sub task id
	 * @return the response entity
	 */
	ResponseEntity<?> lookaheadById(Long taskId, Long subTaskId);
	
	/**
	 * Lookahead pdf report.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the response entity
	 */
	ResponseEntity<?> lookaheadPDFReport(LookaheadListDTO lookaheadListDTO);

	/**
	 * get week start date and leave day of the week.
	 *
	 * @param projectId the project id
	 * @return the project info
	 */
	public ResponseEntity<?> getProjectInfo(Long projectId);
	
	/**
	 *  Insert week start date and leave day of the week.
	 *
	 * @param projectDTO the project DTO
	 * @return the response entity
	 */
	ResponseEntity<?> insertProjectInfo(ProjectDTO projectDTO);

	/**
	 * insert companies based on id.
	 *
	 * @param projectCompanyDTO the project company DTO
	 * @return the response entity
	 */
	ResponseEntity<?> insertProjectCompanyByProjectId(ProjectCompanyDTO projectCompanyDTO);

	/**
	 * Insert trade.
	 *
	 * @param projectDTO the project DTO
	 * @return the response entity
	 */
	ResponseEntity<?> insertTrade(ProjectDTO projectDTO);

	/**
	 * Gets the trade.
	 *
	 * @param projectId the project id
	 * @return the trade
	 */
	ResponseEntity<?> getTrade(Long projectId);

	/**
	 * Download PDF.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @param request the request
	 * @param response the response
	 * @return the response entity
	 */
	ResponseEntity<?> downloadPDF(LookaheadListDTO lookaheadListDTO, HttpServletRequest request, HttpServletResponse response) ;

	/**
	 * Gets the GC by user.
	 *
	 * @return the GC by user
	 */
	ResponseEntity<?> getGCByUser();

	/**
	 * Gets the task summary mail.
	 *
	 * @author MRG
	 * @param projectId the project id
	 * @return the task summary mail
	 */
	ResponseEntity<?> getTaskSummaryMail(Long projectId);

	/**
	 * Insert spec division.
	 *
	 * @author MRG
	 * @param projectDTO the project DTO
	 * @return the response entity
	 */
	ResponseEntity<?> insertSpecDivision(ProjectDTO projectDTO);

	/**
	 * Gets the spec division.
	 *
	 * @author MRG
	 * @param projectId the project id
	 * @return the spec division
	 */
	ResponseEntity<?> getSpecDivision(Long projectId);

	/**
	 * Upload excel to lookahead.
	 *
	 * @param object the object
	 * @param fileName the file name
	 * @param projectId the project id
	 * @param projectCompanyId the project company id
	 * @return the response entity
	 */
	ResponseEntity<?> uploadExcelToLookahead(Object object, String fileName, Long projectId, Long projectCompanyId);

	ResponseEntity<?> getProgressCompletionByProjectId(@Valid ProgressCompletionRequestDTO projectCompletionRequestDTO);

	ResponseEntity<?> downloadPercentageCompletePDF(ProgressCompletionRequestDTO progressCompletionRequestDTO);

	ResponseEntity<?> importExcel(MultipartFile excelFileName, Long projectId, Long projectCompanyId, HttpServletRequest request);

	ResponseEntity<?> formatExcel();

	ResponseEntity<?> importmppFile(MultipartFile mppFileName, Long projectId, Long projectCompanyId,
			HttpServletRequest request);

	ResponseEntity<?> importXerFile(MultipartFile fileName, Long projectId, Long projectCompanyId,
			HttpServletRequest request);

	ResponseEntity<?> importHistory(Long projectId, Integer pageNo, Integer pageSize);
	
	ResponseEntity<?> updateMppFile(MultipartFile fileName,
			 Long projectId, Long projectCompanyId, Long importHistoryId ,HttpServletRequest request);

	ResponseEntity<?> clearSchedule(Long projectId, Long projectCompanyId, Long importHistoryId);

}
