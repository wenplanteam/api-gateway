/**
 * 
 */
package com.mindzen.planner.service;

import org.springframework.http.ResponseEntity;

// TODO: Auto-generated Javadoc
/**
 * The Interface UtilityService.
 *
 * @author Alexpandiyan Chokkan
 * 
 * 25-Sep-2018
 */
public interface UtilityService {

	/**
	 * Load masters.
	 *
	 * @return the response entity
	 */
	public ResponseEntity<?> loadMasters();

	/**
	 * Load time zones.
	 *
	 * @return the response entity
	 */
	public ResponseEntity<?> loadTimeZones();

}
