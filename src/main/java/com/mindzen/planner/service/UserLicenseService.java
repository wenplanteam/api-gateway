/**
 * 
 */
package com.mindzen.planner.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.mindzen.planner.dto.AuthenticationRequest;
import com.mindzen.planner.dto.CompanyDTO;
import com.mindzen.planner.dto.LicenseDistributionDTO;
import com.mindzen.planner.dto.PlanDTO;
import com.mindzen.planner.dto.StripeDataDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.dto.UserPlanMasterDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserLicenseService.
 *
 * @author Alexpandiyan Chokkan
 * 
 * 25-Sep-2018
 */
public interface UserLicenseService {

	/**
	 * Load users.
	 *
	 * @param userId the user id
	 * @param createdBy the created by
	 * @param projectId the project id
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param created the created
	 * @return the response entity
	 */
	public ResponseEntity<?> loadUsers(Long userId, Boolean createdBy, Long projectId, Integer pageNo, Integer pageSize, Long created);
	
	/**
	 * Find by email.
	 *
	 * @param email the email
	 * @return the response entity
	 */
	public ResponseEntity<?> findByEmail(@RequestParam("email") String email);
	
	/**
	 * Creates the user.
	 *
	 * @param userDTO the user DTO
	 * @return the response entity
	 */
	public ResponseEntity<?> createUser(@RequestBody UserDTO userDTO);
	
	/**
	 * Creates the user by basic info.
	 *
	 * @param userDTOs the user DT os
	 * @return the response entity
	 */
	public ResponseEntity<?> createUserByBasicInfo(@RequestBody List<UserDTO> userDTOs);
	
	/**
	 * Verify email.
	 *
	 * @param email the email
	 * @param source the source
	 * @return the string
	 */
	public String verifyEmail(@RequestParam(required = true, value = "email") String email,
										 @RequestParam(required = true, value = "source")String source);
	
	/**
	 * Update user.
	 *
	 * @param userDTO the user DTO
	 * @return the response entity
	 */
	public ResponseEntity<?> updateUser(@RequestBody UserDTO userDTO);
	
	/**
	 * Delete user.
	 *
	 * @param userIds the user id
	 * @return the response entity
	 */
	public ResponseEntity<?> deleteUser(List<Long> userIds);
	
	/**
	 * Decode.
	 *
	 * @param data the data
	 * @return the response entity
	 */
	public ResponseEntity<?> decode(String data);

	/**
	 * List of users.
	 *
	 * @param userDTOs the user DT os
	 * @return the response entity
	 */
	public ResponseEntity<?> listOfUsers(List<UserDTO> userDTOs);

	/**
	 * Find project user by project id.
	 *
	 * @param projectId the project id
	 * @return the response entity
	 */
	public ResponseEntity<?> findProjectUserByProjectId(Long projectId);

	/**
	 * Delete user by email.
	 *
	 * @param email the email
	 * @return the response entity
	 */
	public ResponseEntity<?> deleteUserByEmail(String email);

	/**
	 * Gets the authenticated user.
	 *
	 * @param request the request
	 * @return the authenticated user
	 */
	ResponseEntity<Object> getAuthenticatedUser(HttpServletRequest request);

	/**
	 * Creates the authentication token.
	 *
	 * @param authenticationRequest the authentication request
	 * @return the response entity
	 */
	ResponseEntity<Object> createAuthenticationToken(AuthenticationRequest authenticationRequest);

	/**
	 * Refresh and get authentication token.
	 *
	 * @param request the request
	 * @return the response entity
	 */
	ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request);

	/**
	 * Change password.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param authenticationRequest the authentication request
	 * @return the response entity
	 */
	public ResponseEntity<?> changePassword(HttpServletRequest httpServletRequest, AuthenticationRequest authenticationRequest);

	/**
	 * Forgot password.
	 *
	 * @param authenticationRequest the authentication request
	 * @return the response entity
	 */
	public ResponseEntity<?> forgotPassword(AuthenticationRequest authenticationRequest);

	/**
	 *  Forget Passord Mail verification.
	 *
	 * @param email the email
	 * @param source the source
	 * @return the string
	 */
	public String forgotPassword(String email, String source);

	/**
	 * Company list.
	 *
	 * @param id the id
	 * @param createdBy the created by
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the response entity
	 */
	public ResponseEntity<?> companyList(Long id, Boolean createdBy, Integer pageNo, Integer pageSize);

	/**
	 * Insert company.
	 *
	 * @param companyDTO the company DTO
	 * @return the response entity
	 */
	public ResponseEntity<?> insertCompany(CompanyDTO companyDTO);
	
	/**
	 * Find company by user id.
	 *
	 * @return the response entity
	 */
	public ResponseEntity<?> findCompanyByUserId();

	/**
	 * In activate company.
	 *
	 * @param companyId the company id
	 * @return the response entity
	 */
	public ResponseEntity<?> inActivateCompany(List<Long> companyId);

	/**
	 * Update company.
	 *
	 * @param companyDTO the company DTO
	 * @return the response entity
	 */
	public ResponseEntity<?> updateCompany(CompanyDTO companyDTO);

	/**
	 * Gets the plans.
	 *
	 * @param paid the paid
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param id the id
	 * @return the plans
	 */
	public ResponseEntity<?> getPlans(boolean paid, Integer pageNo, Integer pageSize, Long id);

	/**
	 * Gets the user licenses.
	 *
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param status the status
	 * @return the user licenses
	 */
	public ResponseEntity<?> getUserLicenses(Integer pageNo, Integer pageSize, String status);

	/**
	 * Buy license.
	 *
	 * @param userPlanMasterDTO the user plan master DTO
	 * @return the response entity
	 */
	public ResponseEntity<?> buyLicense(UserPlanMasterDTO userPlanMasterDTO);

	/**
	 * User license count.
	 *
	 * @return the response entity
	 */
	public ResponseEntity<?> userLicenseCount();

	/**
	 * Provide license.
	 *
	 * @param licenseDistributionDTO the license distribution DTO
	 * @return the response entity
	 */
	public ResponseEntity<?> provideLicense(LicenseDistributionDTO licenseDistributionDTO);

	/**
	 * Revoke license.
	 *
	 * @param licenseDistributionDTO the license distribution DTO
	 * @return the response entity
	 */
	public ResponseEntity<?> revokeLicense(LicenseDistributionDTO licenseDistributionDTO);

	/**
	 * Creates the plan.
	 *
	 * @param planDTO the plan DTO
	 * @return the response entity
	 */
	public ResponseEntity<?> createPlan(PlanDTO planDTO);

	/**
	 * Update plan.
	 *
	 * @param planDTO the plan DTO
	 * @return the response entity
	 */
	public ResponseEntity<?> updatePlan(PlanDTO planDTO);

	/**
	 * Delete plan.
	 *
	 * @param planId the plan id
	 * @return the response entity
	 */
	public ResponseEntity<?> deletePlan(List<Long> planId);

	/**
	 * Basic info user create.
	 *
	 * @param userDTO the user DTO
	 * @return the response entity
	 */
	public ResponseEntity<?> basicInfoUserCreate(UserDTO userDTO);

	/**
	 * Find users by email.
	 *
	 * @param email the email
	 * @return the response entity
	 */
	public ResponseEntity<?> findUsersByEmail(String email);

	/**
	 * Gets the license response.
	 *
	 * @param stripeData the stripe data
	 * @return the license response
	 */
	public ResponseEntity<?> getLicenseResponse(StripeDataDTO stripeData);

	/**
	 * Success logout.
	 *
	 * @param request the request
	 * @param response the response
	 * @param authentication the authentication
	 * @return the response entity
	 */
	public ResponseEntity<?> successLogout(HttpServletRequest request, HttpServletResponse response, Authentication authentication);

	/**
	 * Find expiry date.
	 *
	 * @param billingCycle the billing cycle
	 * @return the response entity
	 */
	public ResponseEntity<?> findExpiryDate(String billingCycle);

	/**
	 * Gets the all user informations.
	 *
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the all user informations
	 */
	public ResponseEntity<?> getAllUserInformations(Integer pageNo, Integer pageSize);
	
	
}
