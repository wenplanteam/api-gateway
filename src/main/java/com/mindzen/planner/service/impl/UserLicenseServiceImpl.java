/**
 * 
 */
package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.SUCSFLY;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindzen.infra.api.response.ApiResponse;
import com.mindzen.infra.api.response.ApiResponseConstants;
import com.mindzen.infra.api.response.ApiSuccessResponse;
import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.infra.exception.CustomRuntimeException;
import com.mindzen.planner.config.feign.ProjectManagementFeignClient;
import com.mindzen.planner.config.feign.UserLicenseFeignClient;
import com.mindzen.planner.dto.AuthenticationRequest;
import com.mindzen.planner.dto.CompanyDTO;
import com.mindzen.planner.dto.LicenseDTO;
import com.mindzen.planner.dto.LicenseDistributionDTO;
import com.mindzen.planner.dto.PlanDTO;
import com.mindzen.planner.dto.StripeDataDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.dto.UserPlanMasterDTO;
import com.mindzen.planner.security.factory.AuthenticationResponse;
import com.mindzen.planner.security.factory.CustomUserDetails;
import com.mindzen.planner.security.factory.JwtConfig;
import com.mindzen.planner.security.factory.TokenUtil;
import com.mindzen.planner.service.UserLicenseService;
import com.mindzen.planner.util.DateUtil;
import com.mindzen.planner.util.EncodeDecode;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class UserLicenseServiceImpl.
 *
 * @author Alexpandiyan Chokkan
 * 
 *         25-Sep-2018
 */
@Service

/** The Constant log. */
@Slf4j
public class UserLicenseServiceImpl implements UserLicenseService {

	/** The user license feign client. */
	private UserLicenseFeignClient userLicenseFeignClient;

	/** The jwt config. */
	private JwtConfig jwtConfig;

	/** The token util. */
	private TokenUtil tokenUtil;

	/** The user details service. */
	private UserDetailsService userDetailsService;

	/** The authentication manager. */
	private AuthenticationManager authenticationManager;

	/** The password encoder. */
	private PasswordEncoder passwordEncoder;

	/** The object mapper. */
	private ObjectMapper objectMapper;

	/** The project management feign client. */
	private ProjectManagementFeignClient projectManagementFeignClient;

	/** The registration url. */
	@Value("${registrationUrl}")
	private String registrationUrl;

	/** The login url. */
	@Value("${loginUrl}")
	private String loginUrl;

	/** The confirmation url. */
	@Value("${confirmationUrl}")
	private String confirmationUrl;

	/** The forget password url. */
	@Value("${forgetPasswordUrl}")
	private String forgetPasswordUrl;

	/** The forget password time. */
	@Value("${forgetPasswordTime}")
	private String forgetPasswordTime;

	/**
	 * Instantiates a new user license service impl.
	 *
	 * @param userLicenseFeignClient the user license feign client
	 * @param jwtConfig the jwt config
	 * @param tokenUtil the token util
	 * @param userDetailsService the user details service
	 * @param authenticationManager the authentication manager
	 * @param passwordEncoder the password encoder
	 * @param objectMapper the object mapper
	 * @param projectManagementFeignClient the project management feign client
	 */
	public UserLicenseServiceImpl(UserLicenseFeignClient userLicenseFeignClient, JwtConfig jwtConfig,
			TokenUtil tokenUtil, UserDetailsService userDetailsService, AuthenticationManager authenticationManager,
			PasswordEncoder passwordEncoder, ObjectMapper objectMapper,
			ProjectManagementFeignClient projectManagementFeignClient) {
		super();
		this.userLicenseFeignClient = userLicenseFeignClient;
		this.jwtConfig = jwtConfig;
		this.tokenUtil = tokenUtil;
		this.userDetailsService = userDetailsService;
		this.authenticationManager = authenticationManager;
		this.passwordEncoder = passwordEncoder;
		this.objectMapper = objectMapper;
		this.projectManagementFeignClient = projectManagementFeignClient;
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#getAuthenticatedUser(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public ResponseEntity<Object> getAuthenticatedUser(HttpServletRequest request) {
		String token = request.getHeader(jwtConfig.getHeader()).substring(7);
		String username = tokenUtil.getUsernameFromToken(token);
		CustomUserDetails customUserDetails = (CustomUserDetails) userDetailsService.loadUserByUsername(username);
		ApiResponse response = new ApiSuccessResponse(ApiResponseConstants.SUCS, customUserDetails);
		return new ResponseEntity<>(response,HttpStatus.OK);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#createAuthenticationToken(com.mindzen.planner.dto.AuthenticationRequest)
	 */
	@Override
	public ResponseEntity<Object> createAuthenticationToken(AuthenticationRequest authenticationRequest) {
		authenticationRequest.setEmail(authenticationRequest.getEmail().toLowerCase());
		authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
		final String token = tokenUtil.generateToken(userDetails);
		MSAResponse msaResponse = userLicenseFeignClient.findByEmail(userDetails.getUsername());
		UserDTO userDTO = null;
		if(msaResponse.isSuccess()) {
			userDTO = objectMapper.convertValue(msaResponse.getPayload(), UserDTO.class);
			userDTO.setToken(token);
			MSAResponse response = userLicenseFeignClient.userLicenseAvailability(authenticationRequest.getEmail());
			if(response.isSuccess()) {
				LicenseDTO licenseDTO = objectMapper.convertValue(response.getPayload(), LicenseDTO.class);
				userDTO.setDays(licenseDTO.getDays());
				userDTO.setTrial(licenseDTO.isTrial());
				if(licenseDTO.getDays() > 0)
					userDTO.setLicenseAvailable(true);
				else {
					userDTO.setLicenseAvailable(false);
					userDTO.setDays(0L);
				}
			}
			else
				userDTO.setLicenseAvailable(false);
		}
		return new ResponseEntity<>(new ApiSuccessResponse(ApiResponseConstants.SUCS, userDTO), HttpStatus.OK);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#refreshAndGetAuthenticationToken(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
		String authToken = request.getHeader(jwtConfig.getHeader());

		if(null == authToken)
			throw new CustomRuntimeException("Invalid Token", HttpStatus.UNAUTHORIZED);

		final String token = authToken.substring(7);
		String username = tokenUtil.getUsernameFromToken(token);
		CustomUserDetails customUserDetails = (CustomUserDetails) userDetailsService.loadUserByUsername(username);

		if(tokenUtil.canTokenBeRefreshed(token, customUserDetails.getLastPasswordResetDate())) {
			String refreshedToken = tokenUtil.refreshToken(token);
			ApiResponse response = new ApiSuccessResponse(ApiResponseConstants.SUCS, new AuthenticationResponse(refreshedToken));
			return new ResponseEntity<>(response,HttpStatus.OK);
		}
		else
			throw new CustomRuntimeException("Token cannot be refreshed", HttpStatus.BAD_REQUEST);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#loadUsers(java.lang.Long, java.lang.Boolean, java.lang.Long, java.lang.Integer, java.lang.Integer, java.lang.Long)
	 */
	@Override
	public ResponseEntity<?> loadUsers(Long userId, Boolean createdBy, Long projectId, Integer pageNo, Integer pageSize, Long created) {
		MSAResponse msaResponse = userLicenseFeignClient.loadUsers(userId, createdBy, projectId, pageNo, pageSize, created);
		if (msaResponse.isSuccess())
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#findByEmail(java.lang.String)
	 */
	@Override
	public ResponseEntity<?> findByEmail(String email) {
		MSAResponse msaResponse = userLicenseFeignClient.findByEmail(email.toLowerCase());
		if (msaResponse.isSuccess())
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#createUser(com.mindzen.planner.dto.UserDTO)
	 */
	@Override
	public ResponseEntity<?> createUser(UserDTO userDTO) {
		userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword().trim()));
		userDTO.setEmail(userDTO.getEmail().toLowerCase());
		MSAResponse msaResponse = userLicenseFeignClient.createUser(userDTO);
		if(msaResponse.isSuccess()) {
			MSAResponse userResponse = userLicenseFeignClient.findByEmail(userDTO.getEmail());
			UserDTO user = objectMapper.convertValue(userResponse.getPayload(), UserDTO.class);
			projectManagementFeignClient.userCompanyAssociateToProject(user.getId());
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#createUserByBasicInfo(java.util.List)
	 */
	@Override
	public ResponseEntity<?> createUserByBasicInfo(List<UserDTO> userDTOs) {
		MSAResponse msaResponse = userLicenseFeignClient.createUserByBasicInfo(userDTOs);
		if (msaResponse.isSuccess())
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#verifyEmail(java.lang.String, java.lang.String)
	 */
	@Override
	public String verifyEmail(String email, String source) {
		String decodedEmail = EncodeDecode.decode(email).toLowerCase();
		MSAResponse msaResponse= userLicenseFeignClient.verifyEmail(decodedEmail, source);
		UserDTO userDTO = null;
		if(msaResponse.isSuccess()) {
			userDTO = objectMapper.convertValue(msaResponse.getPayload(), UserDTO.class);
		}
		String url = null ;
		if(null != userDTO) {
			projectManagementFeignClient.updateProjectSharedUserDetails(userDTO);
			String companyName = "";
			String responseEmail ="email=" + decodedEmail;
			if(null != userDTO.getCompanyName())
				companyName =  "&companyName=" + UriUtils.encode(userDTO.getCompanyName(), "UTF-8");
			if(source.equals("forgetPassword")) {
				url = forgetPasswordUrl + "?"+responseEmail;
			}
			else {
				if(userDTO.isActive())
					url = loginUrl + "?"+responseEmail;
				else
					url = registrationUrl + "?source="+source+"&" + responseEmail + companyName ;
			}
		}
		return url;
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#updateUser(com.mindzen.planner.dto.UserDTO)
	 */
	@Override
	public ResponseEntity<?> updateUser(UserDTO userDTO) {
		MSAResponse msaResponse = userLicenseFeignClient.updateUser(userDTO);
		if (msaResponse.isSuccess()) {
			UserDTO user = objectMapper.convertValue(msaResponse.getPayload(), UserDTO.class);
			MSAResponse response = projectManagementFeignClient.updateProjectCompanies(user);
			log.info("response "+response.isSuccess());
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#deleteUser(java.util.List)
	 */
	@Override
	public ResponseEntity<?> deleteUser(List<Long> userId) {
		MSAResponse msaResponse = userLicenseFeignClient.deleteUser(userId);
		if (msaResponse.isSuccess())
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#decode(java.lang.String)
	 */
	@Override
	public ResponseEntity<?> decode(String data) {
		MSAResponse msaResponse = userLicenseFeignClient.decode(data);
		if (msaResponse.isSuccess())
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#listOfUsers(java.util.List)
	 */
	@Override
	public ResponseEntity<?> listOfUsers(List<UserDTO> userDTOs) {
		MSAResponse msaResponse = userLicenseFeignClient.listOfUsers(userDTOs);
		if (msaResponse.isSuccess())
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		else
			throw new CustomRuntimeException(msaResponse);

	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#findProjectUserByProjectId(java.lang.Long)
	 */
	@Override
	public ResponseEntity<?> findProjectUserByProjectId(Long projectId) {
		MSAResponse msaResponse =userLicenseFeignClient.findProjectUserByProjectId(projectId);
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		throw new CustomRuntimeException(msaResponse);	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#deleteUserByEmail(java.lang.String)
	 */
	@Override
	public ResponseEntity<?> deleteUserByEmail(String email) {
		MSAResponse msaResponse = userLicenseFeignClient.deleteUserByEmail(email.toLowerCase());
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		throw new CustomRuntimeException(msaResponse);
	}

	/**
	 * Authenticate.
	 *
	 * @param username the username
	 * @param password the password
	 */
	private void authenticate(String username, String password) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		}
		catch (Exception e) {
			log.info("ERROR :"+e.getMessage());
			loginErroCapture(e);
		}
	}

	private void loginErroCapture(Exception e) {
		if(e.getMessage().contains("Bad credentials"))
			throw new BadCredentialsException("Invalid password");
		else if(e.getMessage().contains("User is disabled"))
			throw new CustomRuntimeException("Registration not yet completed for this user");
		else
			throw new CustomRuntimeException(e.getMessage());
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#changePassword(com.mindzen.planner.dto.PasswordDTO)
	 */
	@Override
	public ResponseEntity<?> changePassword(HttpServletRequest httpServletRequest, AuthenticationRequest authenticationRequest) {

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String password = null ;
		String userName = null;
		MSAResponse msaResponse = null ;
		if(principal instanceof UserDetails) {
			password =  ((UserDetails) principal).getPassword();
			userName = ((UserDetails) principal).getUsername().toLowerCase();
		}
		else
			throw new CustomRuntimeException("TOKEN_EXPIRED", HttpStatus.UNAUTHORIZED);
		if(null != password && passwordEncoder.matches(authenticationRequest.getPassword(), password)) {
			authenticationRequest.setNewPassword(passwordEncoder.encode(authenticationRequest.getNewPassword().trim()));
			authenticationRequest.setEmail(userName);
			msaResponse = userLicenseFeignClient.changePassword(authenticationRequest);
			if(msaResponse.isSuccess()) {
				SecurityContextHolder.clearContext();
				UserDTO userDTO = new UserDTO();
				final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
				final String token = tokenUtil.generateToken(userDetails);
				userDTO.setToken(token);
				msaResponse = new MSAResponse(true, msaResponse.getHttpStatus(), msaResponse.getMessage(),userDTO);
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
				SecurityContextHolder.getContext().setAuthentication(authentication);
				return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
			}
			else
				throw new CustomRuntimeException(msaResponse);
		}
		else
			throw new BadCredentialsException("Password Not Matched");
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#forgotPassword(com.mindzen.planner.dto.AuthenticationRequest)
	 */
	@Override
	public ResponseEntity<?> forgotPassword(AuthenticationRequest authenticationRequest) {
		MSAResponse msaResponse = null;
		if(null != authenticationRequest.getNewPassword() && !authenticationRequest.getNewPassword().isEmpty())
			authenticationRequest.setNewPassword(passwordEncoder.encode(authenticationRequest.getNewPassword().trim()));
		msaResponse = userLicenseFeignClient.forgotPassword(authenticationRequest);
		if(msaResponse.isSuccess()) {
			if(null != authenticationRequest.getNewPassword() && !authenticationRequest.getNewPassword().isEmpty())
				return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
			else
				return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#forgotPassword(java.lang.String, java.lang.String)
	 */
	@Override
	public String forgotPassword(String email, String source) {
		String decodedEmail = EncodeDecode.decode(email).toLowerCase();
		MSAResponse msaResponse = userLicenseFeignClient.findByEmail(decodedEmail);
		UserDTO userDTO = null;
		if(msaResponse.isSuccess()) {
			userDTO = objectMapper.convertValue(msaResponse.getPayload(), UserDTO.class);
			LocalDateTime lastPasswordResetDateTime = DateUtil.getLocalDateTime(userDTO.getLastPasswordResetDate());
			long duration = Duration.between(LocalDateTime.now(), lastPasswordResetDateTime).toMillis();
			if(duration <= Long.parseLong(forgetPasswordTime))
				return forgetPasswordUrl + "?email=" + decodedEmail;
		}
		return forgetPasswordUrl + "?msg=" + "The link has been expired";
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#findCompanyByUserId()
	 */
	@Override
	public ResponseEntity<?> findCompanyByUserId() {
		MSAResponse msaResponse = userLicenseFeignClient.findCompanyByUserId();
		return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#companyList(java.lang.Long, java.lang.Boolean, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public ResponseEntity<?> companyList(Long id, Boolean createdBy, Integer pageNo, Integer pageSize) {
		MSAResponse msaResponse = userLicenseFeignClient.companyList(id,createdBy,pageNo,pageSize);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#insertCompany(com.mindzen.planner.dto.CompanyDTO)
	 */
	@Override
	public ResponseEntity<?> insertCompany(CompanyDTO companyDTO) {
		MSAResponse msaResponse = userLicenseFeignClient.insertCompany(companyDTO);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#updateCompany(com.mindzen.planner.dto.CompanyDTO)
	 */
	@Override
	public ResponseEntity<?> updateCompany(CompanyDTO companyDTO) {
		MSAResponse msaResponse = userLicenseFeignClient.updateCompany(companyDTO);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#inActivateCompany(java.util.List)
	 */
	@Override
	public ResponseEntity<?> inActivateCompany(List<Long> companyId) {
		MSAResponse msaResponse = userLicenseFeignClient.inActivateCompany(companyId);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#getPlans(boolean, java.lang.Integer, java.lang.Integer, java.lang.Long)
	 */
	@Override
	public ResponseEntity<?> getPlans(boolean paid, Integer pageNo, Integer pageSize, Long id) {
		MSAResponse msaResponse =userLicenseFeignClient.loadPlans(paid, pageNo, pageSize, id);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#createPlan(com.mindzen.planner.dto.PlanDTO)
	 */
	@Override
	public ResponseEntity<?> createPlan(PlanDTO planDTO) {
		MSAResponse msaResponse =userLicenseFeignClient.createPlan(planDTO);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#updatePlan(com.mindzen.planner.dto.PlanDTO)
	 */
	@Override
	public ResponseEntity<?> updatePlan(PlanDTO planDTO) {
		MSAResponse msaResponse =userLicenseFeignClient.updatePlan(planDTO);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#deletePlan(java.util.List)
	 */
	@Override
	public ResponseEntity<?> deletePlan(List<Long> planId) {
		MSAResponse msaResponse =userLicenseFeignClient.deletePlan(planId);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#getUserLicenses(java.lang.Integer, java.lang.Integer, java.lang.String)
	 */
	@Override
	public ResponseEntity<?> getUserLicenses(Integer pageNo, Integer pageSize, String status) {
		MSAResponse msaResponse =userLicenseFeignClient.userLicenses(pageNo, pageSize,status);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#buyLicense(com.mindzen.planner.dto.UserPlanMasterDTO)
	 */
	@Override
	public ResponseEntity<?> buyLicense(UserPlanMasterDTO userPlanMasterDTO) {
		MSAResponse msaResponse =userLicenseFeignClient.buyLicense(userPlanMasterDTO);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#userLicenseCount()
	 */
	@Override
	public ResponseEntity<?> userLicenseCount() {
		MSAResponse msaResponse =userLicenseFeignClient.userLicenseCount();
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#provideLicense(com.mindzen.planner.dto.LicenseDistributionDTO)
	 */
	@Override
	public ResponseEntity<?> provideLicense(LicenseDistributionDTO licenseDistributionDTO) {
		MSAResponse msaResponse =userLicenseFeignClient.provideLicense(licenseDistributionDTO);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#revokeLicense(com.mindzen.planner.dto.LicenseDistributionDTO)
	 */
	@Override
	public ResponseEntity<?> revokeLicense(LicenseDistributionDTO licenseDistributionDTO) {
		MSAResponse msaResponse =userLicenseFeignClient.revokeLicense(licenseDistributionDTO);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#basicInfoUserCreate(com.mindzen.planner.dto.UserDTO)
	 */
	@Override
	public ResponseEntity<?> basicInfoUserCreate(UserDTO userDTO) {
		MSAResponse msaResponse =userLicenseFeignClient.createBasicUser(userDTO);
		if (msaResponse.isSuccess()) {
			/*UserDTO userDTOs = objectMapper.convertValue(msaResponse.getPayload(), UserDTO.class);
			if(null != userDTOs.getProjectId()) {
				userDTOs.setActive(false);
				userDTOs.setEmailVerified(false);
				userDTOs.setEmailSend(true);
				ProjectCompanyDTO projectCompanyDTO =  new ProjectCompanyDTO();
				projectCompanyDTO.setProjectId(userDTOs.getProjectId());
				CompanyDTO companyDTO = new CompanyDTO();
				companyDTO.setId(userDTOs.getCompanyId());
				companyDTO.setProjectRole(Constants.SUB_CONTRACTOR);
				List<CompanyDTO> companyDTOs = new ArrayList<>();
				companyDTOs.add(companyDTO);
				projectCompanyDTO.setProjectCompanies(companyDTOs);
				MSAResponse response = projectManagementFeignClient.insertProjectCompanies(projectCompanyDTO);
				List<Integer> projectCompanyIds = objectMapper.convertValue(response.getPayload(), List.class);
				ProjectUserDTO projectUserDTO = new ProjectUserDTO();
				projectUserDTO.setProjectId(userDTOs.getProjectId());
				Integer id = projectCompanyIds.get(0);
				Long projectCompanyId = new Long(id);
				userDTOs.setProjectCompanyId(projectCompanyId);
				List<UserDTO> userDTOList = new ArrayList<>();
				userDTOList.add(userDTOs);
				projectUserDTO.setProjectUsers(userDTOList);
				MSAResponse projectMSAResponse = projectManagementFeignClient.shareProjectToUser(projectUserDTO);
			}*/
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#findUsersByEmail(java.lang.String)
	 */
	@Override
	public ResponseEntity<?> findUsersByEmail(String email) {
		MSAResponse msaResponse =userLicenseFeignClient.findUsersByEmail(email.toLowerCase());
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#getLicenseResponse(com.mindzen.planner.dto.StripeDataDTO)
	 */
	@Override
	public ResponseEntity<?> getLicenseResponse(StripeDataDTO stripeData) {
		MSAResponse msaResponse =userLicenseFeignClient.getLicenseResponse(stripeData);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#successLogout(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.Authentication)
	 */
	@Override
	public ResponseEntity<?> successLogout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		System.out.println("logout");
		try {
			HttpSession session= request.getSession(false);
			SecurityContextHolder.clearContext();
			session= request.getSession(false);
			if(session != null) {
				session.invalidate();
			}
			for(Cookie cookie : request.getCookies()) {
				cookie.setMaxAge(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		MSAResponse msaResponse = new MSAResponse(true, HttpStatus.OK, SUCSFLY, null);
		return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#findExpiryDate(java.lang.String)
	 */
	@Override
	public ResponseEntity<?> findExpiryDate(String billingCycle) {
		MSAResponse msaResponse = userLicenseFeignClient.findExpirydate(billingCycle);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UserLicenseService#getAllUserInformations(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public ResponseEntity<?> getAllUserInformations(Integer pageNo, Integer pageSize) {
		MSAResponse msaResponse = userLicenseFeignClient.getAllUserInformations(pageNo, pageSize);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

}