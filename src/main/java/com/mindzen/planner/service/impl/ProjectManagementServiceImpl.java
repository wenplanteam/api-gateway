/**
 * 
 */
package com.mindzen.planner.service.impl;

import static com.mindzen.infra.api.response.ApiResponseConstants.SUCSFLY;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mindzen.infra.api.response.ApiSuccessResponse;
import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.infra.exception.CustomRuntimeException;
import com.mindzen.planner.config.feign.ProjectManagementFeignClient;
import com.mindzen.planner.config.feign.UserLicenseFeignClient;
import com.mindzen.planner.config.feign.UtilityFeignClient;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.LookaheadJasperReportRootDTO;
import com.mindzen.planner.dto.LookaheadListDTO;
import com.mindzen.planner.dto.PercentCompleteJasperReportDetailRootDTO;
import com.mindzen.planner.dto.ProgressCompletionRequestDTO;
import com.mindzen.planner.dto.ProjectCompanyDTO;
import com.mindzen.planner.dto.ProjectDTO;
import com.mindzen.planner.dto.ProjectUserDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.service.ProjectManagementService;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class ProjectManagementServiceImpl.
 *
 * @author Alexpandiyan Chokkan
 * 
 *         25-Sep-2018
 */
@Service
@Slf4j
/** The Constant log. */
public class ProjectManagementServiceImpl implements ProjectManagementService {

	/** The project management feign client. */
	private ProjectManagementFeignClient projectManagementFeignClient;

	/** The user license feign client. */
	private UserLicenseFeignClient userLicenseFeignClient;

	/** The utility feign client. */
	private UtilityFeignClient utilityFeignClient;

	/** The object mapper. */
	private ObjectMapper objectMapper;

	@Value("${upload.file.directory}")
	private String uploadFileDirectory;

	/**
	 * Instantiates a new project management service impl.
	 *
	 * @param projectManagementFeignClient the project management feign client
	 * @param userLicenseFeignClient the user license feign client
	 * @param objectMapper the object mapper
	 * @param utilityFeignClient the utility feign client
	 */
	public ProjectManagementServiceImpl(ProjectManagementFeignClient projectManagementFeignClient,
			UserLicenseFeignClient userLicenseFeignClient, ObjectMapper objectMapper ,UtilityFeignClient utilityFeignClient) {
		this.projectManagementFeignClient = projectManagementFeignClient;
		this.userLicenseFeignClient = userLicenseFeignClient;
		this.objectMapper = objectMapper;
		this.utilityFeignClient = utilityFeignClient;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.mindzen.planner.service.ProjectManagementService#shareProject(java.util.
	 * List)
	 */
	@Override
	public ResponseEntity<?> shareProject(ProjectUserDTO projectUserDTO) {
		MSAResponse deleteUserResponse = null ;
		List<UserDTO> deleteUserList = null;
		if(projectUserDTO.getDeleteUsers() !=  null) {
			deleteUserResponse = userLicenseFeignClient.findUsersByIds(projectUserDTO.getDeleteUsers());
			deleteUserList = objectMapper.convertValue(deleteUserResponse.getPayload(), List.class);
			projectUserDTO.setDeleteUserDTO(deleteUserList);
		}
		List<UserDTO> userDTOs = null;
		MSAResponse userMSAResponse = userLicenseFeignClient.createUserByBasicInfo(projectUserDTO.getProjectUsers());
		MSAResponse projectMSAResponse = null;
		if (userMSAResponse.isSuccess()) {
			userDTOs = objectMapper.convertValue(userMSAResponse.getPayload(), List.class);
			projectUserDTO.setProjectUsers(userDTOs);
			projectMSAResponse = projectManagementFeignClient.shareProjectToUser(projectUserDTO);
			if(!projectMSAResponse.isSuccess())
				throw new CustomRuntimeException(projectMSAResponse);
		} else
			throw new CustomRuntimeException(userMSAResponse);
		return new ResponseEntity<>(projectMSAResponse, HttpStatus.OK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mindzen.planner.service.ProjectManagementService#createProject(com.
	 * mindzen.planner.dto.ProjectDTO)
	 */
	@Override
	public ResponseEntity<?> createProject(ProjectDTO projectDTO) {
		//	MSAResponse userMSAResponse = userLicenseFeignClient.findLicenseDistributionId(); // license based project counts
		Long licenseDistributionId = 1L;
		//		if(userMSAResponse.isSuccess())
		//			licenseDistributionId = ((Integer) userMSAResponse.getPayload()).longValue(); // Inportant of need license dist id
		projectDTO.setLicenseDistributionId(licenseDistributionId);

		/*		MSAResponse companyResponse = userLicenseFeignClient.findCompanyByUserId();
		CompanyDTO companyDTO = null;
		if(companyResponse.isSuccess())
			companyDTO = objectMapper.convertValue(companyResponse.getPayload(), CompanyDTO.class);*/

		MSAResponse msaResponse = projectManagementFeignClient.createProject(projectDTO);
		if (msaResponse.isSuccess())
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#updateProject(com.mindzen.planner.dto.ProjectDTO)
	 */
	@Override
	public ResponseEntity<?> updateProject(ProjectDTO projectDTO) {
		MSAResponse msaResponse = projectManagementFeignClient.updateProject(projectDTO);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		} else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#deleteProject(java.lang.Long[])
	 */
	@Override
	public ResponseEntity<?> deleteProject(Long[] projectId) {
		MSAResponse msaResponse = projectManagementFeignClient.deleteProject(projectId);
		if (msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		} else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#loadAllProjects(java.lang.Integer, java.lang.Integer, java.lang.Boolean, java.lang.String, java.lang.Long, java.lang.Long)
	 */
	@Override
	public ResponseEntity<?> loadAllProjects(Integer pageNo, Integer pageSize, Boolean status, String searchTerm, 
			Long projectId, Long created) {
		if(null != projectId) {
			MSAResponse msaResponse = projectManagementFeignClient.findByProjectId(projectId);
			if (msaResponse.isSuccess()) {
				return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
			} else {
				throw new CustomRuntimeException(msaResponse);
			}
		}
		else {
			MSAResponse msaResponse = projectManagementFeignClient.loadAllProjects(pageNo, pageSize, status, searchTerm,created);
			if (msaResponse.isSuccess()) {
				return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
			} else
				throw new CustomRuntimeException(msaResponse);
		}
	}

	/*@Override
	public ResponseEntity<?> findByProjectId(Long projectId) {

	}*/



	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#createLookahead(com.mindzen.planner.dto.LookaheadDTO)
	 */
	@Override
	public ResponseEntity<?> createLookahead(LookaheadDTO lookaheadDTO) {
		MSAResponse msaResponse = projectManagementFeignClient.createLookahead(lookaheadDTO);
		if (msaResponse.isSuccess())
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#listLookahead(com.mindzen.planner.dto.LookaheadListDTO)
	 */
	@Override
	public ResponseEntity<?> listLookahead(LookaheadListDTO lookaheadListDTO) {
		MSAResponse msaResponse = projectManagementFeignClient.listLookahead(lookaheadListDTO);
		ResponseEntity<?> response = new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		if (msaResponse.isSuccess())
			return response;
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#findByProjectForPlanSharing(java.lang.Long)
	 */
	@Override
	public ResponseEntity<?> findByProjectForPlanSharing(Long projectId) {
		MSAResponse msaResponse = projectManagementFeignClient.findByProjectForPlanSharing(projectId);
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#lookaheadInfo(java.lang.Long)
	 */
	@Override
	public ResponseEntity<?> lookaheadInfo(Long projectId) {
		MSAResponse msaResponse = projectManagementFeignClient.lookaheadInfo(projectId);
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#updateLookahead(com.mindzen.planner.dto.LookaheadDTO)
	 */
	@Override
	public ResponseEntity<?> updateLookahead(LookaheadDTO lookaheadDTO) {
		MSAResponse msaResponse = projectManagementFeignClient.updateLookahead(lookaheadDTO);
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#lookaheadById(java.lang.Long, java.lang.Long)
	 */
	@Override
	public ResponseEntity<?> lookaheadById(Long taskId, Long subTaskId) {
		MSAResponse msaResponse = projectManagementFeignClient.lookaheadById(taskId,subTaskId);
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#lookaheadPDFReport(com.mindzen.planner.dto.LookaheadDTO)
	 */
	@Override
	public ResponseEntity<?> lookaheadPDFReport(LookaheadListDTO lookaheadListDTO) {
		MSAResponse msaResponse = projectManagementFeignClient.lookaheadPDFReport(lookaheadListDTO);
		if(msaResponse.isSuccess()) {
			msaResponse = new MSAResponse(true, HttpStatus.OK, "Mail Sent "+SUCSFLY, null);
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#getProjectInfo(java.lang.Long)
	 */
	@Override
	public ResponseEntity<?> getProjectInfo(Long projectId) {
		MSAResponse msaResponse = projectManagementFeignClient.getProjectInfo(projectId);
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#insertProjectInfo(com.mindzen.planner.dto.ProjectDTO)
	 */
	@Override
	public ResponseEntity<?> insertProjectInfo(ProjectDTO projectDTO) {
		MSAResponse msaResponse = projectManagementFeignClient.insertProjectInfo(projectDTO);
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#insertProjectCompanyByProjectId(com.mindzen.planner.dto.ProjectCompanyDTO)
	 */
	@Override
	public ResponseEntity<?> insertProjectCompanyByProjectId(ProjectCompanyDTO projectCompanyDTO) {
		MSAResponse msaResponse = projectManagementFeignClient.insertProjectCompanies(projectCompanyDTO);
		if(msaResponse.isSuccess()) {
			msaResponse =new MSAResponse(true, HttpStatus.OK, "Companies Associated Successfully", null);
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#findProjectCompanyByProjectId(java.lang.Long)
	 */
	@Override
	public ResponseEntity<?> findProjectCompanyByProjectId(Long projectId) {
		MSAResponse msaResponse = projectManagementFeignClient.findProjectCompanyByProjectId(projectId);
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#insertTrade(com.mindzen.planner.dto.ProjectDTO)
	 */
	@Override
	public ResponseEntity<?> insertTrade(ProjectDTO projectDTO) {
		MSAResponse msaResponse = projectManagementFeignClient.insertTrade(projectDTO);
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#getTrade(java.lang.Long)
	 */
	@Override
	public ResponseEntity<?> getTrade(Long projectId) {
		MSAResponse msaResponse = projectManagementFeignClient.getTrade(projectId);
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#downloadPDF(com.mindzen.planner.dto.LookaheadListDTO, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public ResponseEntity<?> downloadPDF(LookaheadListDTO lookaheadListDTO, HttpServletRequest request, HttpServletResponse response) {
		MSAResponse msaResponse = projectManagementFeignClient.lookaheadPDFReport(lookaheadListDTO);
		if(msaResponse.isSuccess()) {
			LookaheadJasperReportRootDTO lookaheadJasperReportRootDTO = objectMapper.convertValue(msaResponse.getPayload(), LookaheadJasperReportRootDTO.class);
			if(null != lookaheadJasperReportRootDTO) {
				MSAResponse utilityResponse = utilityFeignClient.pdfGeneration(lookaheadJasperReportRootDTO);
				if(utilityResponse.isSuccess()) {
					byte[] fileContent = objectMapper.convertValue(utilityResponse.getPayload(), byte[].class);
					String pdfFileName = "lookahead-"+System.currentTimeMillis()+".pdf";
					try {
						InputStream inputStream = new ByteArrayInputStream(fileContent);
						byte[] data = IOUtils.toByteArray(inputStream);
						ByteArrayResource resource = new ByteArrayResource(data);
						return ResponseEntity.ok()
								.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + pdfFileName)
								.contentType(MediaType.APPLICATION_PDF) //
								.contentLength(data.length) //
								.body(resource);
					} catch(FileNotFoundException e) {
						msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND, "IOUtils fiel not found error", "IOUtils error", null);
						throw new CustomRuntimeException(msaResponse);
					}
					catch(IOException e) {
						msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND, "file error" , "E-001", null);
						throw new CustomRuntimeException(msaResponse);
					}
				}
				else {
					msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND, "utility error", "E-001", null);
					throw new CustomRuntimeException(msaResponse);
				}
			}
			else {
				msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND, "payload error", "E-001", null);
				throw new CustomRuntimeException(msaResponse);
			}
		}
		else {
			throw new CustomRuntimeException(msaResponse);
		}


	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#getGCByUser()
	 */
	@Override
	public ResponseEntity<?> getGCByUser() {
		MSAResponse msaResponse = projectManagementFeignClient.getGCByUser();
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(msaResponse);
		}
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.ProjectManagementService#getTaskSummaryMail(java.lang.Long)
	 */
	@Override
	public ResponseEntity<?> getTaskSummaryMail(Long projectId) {
		MSAResponse response = projectManagementFeignClient.getTaskSummaryMail(projectId);
		if(response.isSuccess()) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(response);
		}
	}

	@Override
	public ResponseEntity<?> insertSpecDivision(ProjectDTO projectDTO) {
		MSAResponse response = projectManagementFeignClient.insertSpecDivision(projectDTO);
		if(response.isSuccess()) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(response);
		}
	}

	@Override
	public ResponseEntity<?> getSpecDivision(Long projectId) {
		MSAResponse response = projectManagementFeignClient.getSpecDivision(projectId);
		if(response.isSuccess()) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(response);
		}
	}

	@Override
	public ResponseEntity<?> uploadExcelToLookahead(Object object, String fileName, Long projectId,
			Long projectCompanyId) {
		MSAResponse response = projectManagementFeignClient.uploadLookaheadExcel(fileName, projectId, projectCompanyId);
		if(response.isSuccess()) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(response);
		}
	}

	public ResponseEntity<?> getProgressCompletionByProjectId(
			@Valid ProgressCompletionRequestDTO projectCompletionRequestDTO) {
		MSAResponse response = projectManagementFeignClient.getProgressCompletionByProjectId(projectCompletionRequestDTO);
		if(response.isSuccess()) {
			return new ResponseEntity<>(response, HttpStatus.OK);
		}else {
			throw new CustomRuntimeException(response);
		}
	}

	@Override
	public ResponseEntity<?> downloadPercentageCompletePDF(ProgressCompletionRequestDTO progressCompletionRequestDTO) {
		MSAResponse msaResponse = projectManagementFeignClient.downloadPercentageCompletePDF(progressCompletionRequestDTO);
		if(msaResponse.isSuccess() && progressCompletionRequestDTO.isDownload()) {
			PercentCompleteJasperReportDetailRootDTO percentCompleteReportRootDTO = 
					objectMapper.convertValue(msaResponse.getPayload(), PercentCompleteJasperReportDetailRootDTO.class);
			if(null != percentCompleteReportRootDTO) {
				MSAResponse utilityResponse = utilityFeignClient.generatePercentageCompleteReport(percentCompleteReportRootDTO);
				if(utilityResponse.isSuccess()) {
					byte[] fileContent = objectMapper.convertValue(utilityResponse.getPayload(), byte[].class);
					String pdfFileName = "percentComplete-"+System.currentTimeMillis()+".pdf";
					try {
						InputStream inputStream = new ByteArrayInputStream(fileContent);
						byte[] data = IOUtils.toByteArray(inputStream);
						ByteArrayResource resource = new ByteArrayResource(data);
						return ResponseEntity.ok()
								.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + pdfFileName)
								.contentType(MediaType.APPLICATION_PDF) //
								.contentLength(data.length) //
								.body(resource);
					} catch(FileNotFoundException e) {
						msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND, "IOUtils fiel not found error", "IOUtils error", null);
						throw new CustomRuntimeException(msaResponse);
					}
					catch(IOException e) {
						msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND, "file error" , "E-001", null);
						throw new CustomRuntimeException(msaResponse);
					}
				}
				else {
					msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND, "utility error", "E-001", null);
					throw new CustomRuntimeException(msaResponse);
				}
			}
			else {
				msaResponse = new MSAResponse(false, HttpStatus.NOT_FOUND, "payload error", "E-001", null);
				throw new CustomRuntimeException(msaResponse);
			}
		}
		else if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(msaResponse, HttpStatus.OK);
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	@Override
	public ResponseEntity<?> importExcel(MultipartFile excelFileName, Long projectId, Long projectCompanyId, 
			HttpServletRequest request) {
		MSAResponse response = new MSAResponse(false, HttpStatus.BAD_REQUEST, "excel data conversion error", null);
		String contentTypeForHeaders = "application/octet-stream";
		try {
			String fileName= excelFileName.getOriginalFilename();
			String fileNameForServer = System.currentTimeMillis()+"_"+fileName;
			Path path = Paths.get(uploadFileDirectory+fileNameForServer);
			byte[] bytes =excelFileName.getBytes();
			Files.write(path, bytes);
			response = projectManagementFeignClient.importExcel(fileNameForServer,projectId,projectCompanyId);
			List<String> errorList = objectMapper.convertValue(response.getPayload(), List.class);
			String errorDestFileName = response.getErrorCode();
			if(!response.isSuccess()) {
				String contentType = "";
				if(response.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED) || response.getMessage().contains("missing")) {
					try {
						contentType = request.getServletContext().getMimeType(path.toString());
					} catch (Exception ex) {
						log.info("Could not determine file type.");
					}
					if(contentType == null || (contentType != null && contentType.equals("")))
						contentType = contentTypeForHeaders;
					byte[] fileContent = Files.readAllBytes(path);
					Files.delete(path);
					HttpHeaders headers = new HttpHeaders();
					headers.add("Message", response.getMessage());
					headers.add("Code", "400");
					return ResponseEntity.ok()
							.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileNameForServer)
							.contentType(MediaType.parseMediaType(contentType))
							.headers(headers)
							.body(fileContent);
				}
				else {
					Path destErrorPath = Paths.get(uploadFileDirectory+errorDestFileName);
					byte[] fileContent = Files.readAllBytes(destErrorPath);
					Files.delete(destErrorPath);
					Files.delete(path);
					if(contentType == null || (contentType != null && contentType.equals("")))
						contentType = contentTypeForHeaders;
					HttpHeaders headers = new HttpHeaders();
					headers.add("Message", response.getMessage());
					headers.add("Code", "400");
					return ResponseEntity.ok()
							.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + errorDestFileName)
							.contentType(MediaType.parseMediaType(contentType))
							.headers(headers)
							.body(fileContent);
				}
			}
			else if(errorList != null && !errorList.isEmpty()) {
				Path destErrorPath = Paths.get(uploadFileDirectory+errorDestFileName);
				byte[] fileContent = Files.readAllBytes(destErrorPath);
				Files.delete(destErrorPath);
				HttpHeaders headers = new HttpHeaders();
				headers.add("Message", response.getMessage());
				headers.add("Code", "400");
				return ResponseEntity.ok()
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + errorDestFileName)
						.contentType(MediaType.parseMediaType(contentTypeForHeaders))
						.headers(headers)
						.body(fileContent);
			}
			else {
				Files.delete(path);
				HttpHeaders headers = new HttpHeaders();
				headers.add("Message", response.getMessage());
				headers.add("Code", "200");
				return ResponseEntity.ok()
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + errorDestFileName)
						.contentType(MediaType.APPLICATION_JSON)
						.headers(headers)
						.body(response);
			}
		} catch (Exception e) {
			if(excelFileName == null)
				throw new CustomRuntimeException("Multipart file not found", HttpStatus.BAD_REQUEST);
			throw new CustomRuntimeException(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<?> formatExcel() {
		try {
			Path fromatExcelPath = Paths.get("/home/mindzen/wenplan.xlsx");
			byte[] fileContent = Files.readAllBytes(fromatExcelPath);
			String fileName = "ImportTasksTemplate.xlsx";
			return ResponseEntity.ok()
					.contentType(MediaType.parseMediaType("application/octet-stream"))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
					.body(fileContent);
		}
		catch (Exception e) {
			throw new CustomRuntimeException("Excel not found", HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<?> importmppFile(MultipartFile mppFileName, Long projectId, Long projectCompanyId,
			HttpServletRequest request) {
		try {
			String contentTypeForHeaders = "application/octet-stream";
			String fileName= mppFileName.getOriginalFilename();
			fileName = fileName.replaceAll("\\s+", "");
			String fileNameForServer = System.currentTimeMillis()+"_"+fileName;
			Path path = Paths.get(uploadFileDirectory+fileNameForServer);
			byte[] bytes =mppFileName.getBytes();
			Files.write(path, bytes);
			MSAResponse response = projectManagementFeignClient.importMppFile(fileNameForServer,projectId,projectCompanyId);
			List<String> errorList = objectMapper.convertValue(response.getPayload(), List.class);
			String errorDestFileName = response.getErrorCode();
			if(!response.isSuccess()) {
				String contentType = "";
				if(response.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED) || response.getMessage().contains("missing")) {
					try {
						contentType = request.getServletContext().getMimeType(path.toString());
					} catch (Exception ex) {
						log.info("Could not determine file type.");
					}
					if(contentType == null || (contentType != null && contentType.equals("")))
						contentType = contentTypeForHeaders;
					byte[] fileContent = Files.readAllBytes(path);
					Files.delete(path);
					HttpHeaders headers = new HttpHeaders();
					headers.add("Message", response.getMessage());
					headers.add("Code", "400");
					headers.add("fileType","mpp");
					return ResponseEntity.ok()
							.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileNameForServer)
							.contentType(MediaType.parseMediaType(contentType))
							.headers(headers)
							.body(fileContent);
				}
				else {
					Path destErrorPath = Paths.get(uploadFileDirectory+errorDestFileName);
					byte[] fileContent = Files.readAllBytes(destErrorPath);
					Files.delete(destErrorPath);
					Files.delete(path);
					if(contentType == null || (contentType != null && contentType.equals("")))
						contentType = contentTypeForHeaders;
					HttpHeaders headers = new HttpHeaders();
					headers.add("Message", response.getMessage());
					headers.add("Code", "400");
					headers.add("fileType","mpp");
					return ResponseEntity.ok()
							.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + errorDestFileName)
							.contentType(MediaType.parseMediaType(contentType))
							.headers(headers)
							.body(fileContent);
				}
			}
			else if(errorList != null && !errorList.isEmpty()) {
				Path destErrorPath = Paths.get(uploadFileDirectory+errorDestFileName);
				byte[] fileContent = Files.readAllBytes(destErrorPath);
				Files.delete(destErrorPath);
				HttpHeaders headers = new HttpHeaders();
				headers.add("Message", response.getMessage());
				headers.add("Code", "400");
				headers.add("fileType","mpp");
				return ResponseEntity.ok()
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + errorDestFileName)
						.contentType(MediaType.parseMediaType(contentTypeForHeaders))
						.headers(headers)
						.body(fileContent);
			}
			else {
				Files.delete(path);
				HttpHeaders headers = new HttpHeaders();
				headers.add("Message", response.getMessage());
				headers.add("Code", "200");
				return ResponseEntity.ok()
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + errorDestFileName)
						.contentType(MediaType.APPLICATION_JSON)
						.headers(headers)
						.body(response);
			}
		} catch (Exception e) {
			if(mppFileName == null)
				throw new CustomRuntimeException("Multipart file not found", HttpStatus.BAD_REQUEST);
			throw new CustomRuntimeException(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<?> importXerFile(MultipartFile xerFileName, Long projectId, Long projectCompanyId,
			HttpServletRequest request) {
		try {
			String fileName= xerFileName.getOriginalFilename();
			String fileNameForServer = System.currentTimeMillis()+"_"+fileName;
			Path path = Paths.get(uploadFileDirectory+fileNameForServer);
			byte[] bytes =xerFileName.getBytes();
			Files.write(path, bytes);
			MSAResponse response = projectManagementFeignClient.importXerFile(fileNameForServer,projectId,projectCompanyId);
			Files.delete(path);
			HttpHeaders headers = new HttpHeaders();
			headers.add("Message", response.getMessage());
			headers.add("Code", "200");
			String errorDestFileName = response.getErrorCode();
			return ResponseEntity.ok()
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + errorDestFileName)
					.contentType(MediaType.APPLICATION_JSON)
					.headers(headers)
					.body(response);
			//return new ResponseEntity<>(response, HttpStatus.OK);
		}
		catch (Exception e) {
			log.info("Mpp File conversion error"+e.getLocalizedMessage());
			throw new CustomRuntimeException("Mpp file conversion error", HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<?> importHistory(Long projectId,Integer pageNo,Integer pageSize) {
		MSAResponse response = projectManagementFeignClient.importHistory(projectId,pageNo,pageSize);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> updateMppFile(MultipartFile mppFileName, Long projectId, Long projectCompanyId,
			Long importHistoryId, HttpServletRequest request) {
		try {
			String contentTypeForHeaders = "application/octet-stream";
			String fileName= mppFileName.getOriginalFilename();
			fileName = fileName.replaceAll("\\s+", "");
			String fileNameForServer = System.currentTimeMillis()+"_"+fileName;
			Path path = Paths.get(uploadFileDirectory+fileNameForServer);
			byte[] bytes =mppFileName.getBytes();
			Files.write(path, bytes);
			MSAResponse response =projectManagementFeignClient.updateMppFile(fileNameForServer, projectId, projectCompanyId, importHistoryId);
			List<String> errorList = objectMapper.convertValue(response.getPayload(), List.class);
			String errorDestFileName = response.getErrorCode();
			if(!response.isSuccess()) {
				String contentType = "";
				if(response.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED) || response.getMessage().contains("missing")) {
					try {
						contentType = request.getServletContext().getMimeType(path.toString());
					} catch (Exception ex) {
						log.info("Could not determine file type.");
					}
					if(contentType == null || (contentType != null && contentType.equals("")))
						contentType = contentTypeForHeaders;
					byte[] fileContent = Files.readAllBytes(path);
					Files.delete(path);
					HttpHeaders headers = new HttpHeaders();
					headers.add("Message", response.getMessage());
					headers.add("Code", "400");
					return ResponseEntity.ok()
							.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileNameForServer)
							.contentType(MediaType.parseMediaType(contentType))
							.headers(headers)
							.body(fileContent);
				}
				else {
					Path destErrorPath = Paths.get(uploadFileDirectory+errorDestFileName);
					byte[] fileContent = Files.readAllBytes(destErrorPath);
					Files.delete(destErrorPath);
					Files.delete(path);
					if(contentType == null || (contentType != null && contentType.equals("")))
						contentType = contentTypeForHeaders;
					HttpHeaders headers = new HttpHeaders();
					headers.add("Message", response.getMessage());
					headers.add("Code", "400");
					return ResponseEntity.ok()
							.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + errorDestFileName)
							.contentType(MediaType.parseMediaType(contentType))
							.headers(headers)
							.body(fileContent);
				}
			}
			else if(errorList != null && !errorList.isEmpty()) {
				Path destErrorPath = Paths.get(uploadFileDirectory+errorDestFileName);
				byte[] fileContent = Files.readAllBytes(destErrorPath);
				Files.delete(destErrorPath);
				HttpHeaders headers = new HttpHeaders();
				headers.add("Message", response.getMessage());
				headers.add("Code", "400");
				return ResponseEntity.ok()
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + errorDestFileName)
						.contentType(MediaType.parseMediaType(contentTypeForHeaders))
						.headers(headers)
						.body(fileContent);
			}
			else {
				Files.delete(path);
				HttpHeaders headers = new HttpHeaders();
				headers.add("Message", response.getMessage());
				headers.add("Code", "200");
				return ResponseEntity.ok()
						.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + errorDestFileName)
						.contentType(MediaType.APPLICATION_JSON)
						.headers(headers)
						.body(response);
			}
		} catch (Exception e) {
			if(mppFileName == null)
				throw new CustomRuntimeException("Multipart file not found", HttpStatus.BAD_REQUEST);
			throw new CustomRuntimeException(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
		}
	}


	@Override
	public ResponseEntity<?> clearSchedule(Long projectId, Long projectCompanyId, Long importHistoryId) {
		try {
			MSAResponse response =projectManagementFeignClient.clearSchedule( projectId, projectCompanyId, importHistoryId);
			HttpHeaders headers = new HttpHeaders();
			headers.add("Message", response.getMessage());
			headers.add("Code", "200");
		return ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.headers(headers)
				.body(response);
	}catch (Exception e) {
		log.info(""+e.getLocalizedMessage());
		throw new CustomRuntimeException("error", HttpStatus.BAD_REQUEST);
	}

}
	/*
	@Override
	public ResponseEntity<?> findProjectCompanyByProjectId(Long projectId) {
		MSAResponse projectMsaResponse = projectManagementFeignClient.findProjectCompanyByProjectId(projectId);
		MSAResponse userMsaResponse = userLicenseFeignClient.companyList(null);
		Map<String, Object> companies = new HashMap<>();
		if(userMsaResponse.isSuccess() && projectMsaResponse.isSuccess()) {
			ProjectCompanyDTO projectCompany = objectMapper.convertValue(projectMsaResponse.getPayload(), ProjectCompanyDTO.class);
			companies.put("projectCompanies", projectCompany.getProjectCompanies());
			companies.put("companies", userMsaResponse.getPayload());
			companies.put("projectId", projectCompany.getProjectId());
			ApiResponse apiResponse = new ApiSuccessResponse("SUCCESS", null, companies);
			return new ResponseEntity<>(apiResponse, HttpStatus.OK);
		}
		else {
			throw new CustomRuntimeException(projectMsaResponse);
		}
	}
	 */
}
