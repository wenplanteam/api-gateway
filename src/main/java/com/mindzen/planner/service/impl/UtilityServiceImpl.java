/**
 * 
 */
package com.mindzen.planner.service.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mindzen.infra.api.response.ApiSuccessResponse;
import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.infra.exception.CustomRuntimeException;
import com.mindzen.planner.config.feign.UtilityFeignClient;
import com.mindzen.planner.service.UtilityService;

// TODO: Auto-generated Javadoc
/**
 * The Class UtilityServiceImpl.
 *
 * @author Alexpandiyan Chokkan
 * 
 * 25-Sep-2018
 */
@Service
public class UtilityServiceImpl implements UtilityService {

	/** The utility feign client. */
	private UtilityFeignClient utilityFeignClient;

	/**
	 * Instantiates a new utility service impl.
	 *
	 * @param utilityFeignClient the utility feign client
	 */
	public UtilityServiceImpl(UtilityFeignClient utilityFeignClient) {
		this.utilityFeignClient = utilityFeignClient;
	}
	
	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UtilityService#loadMasters()
	 */
	@Override
	public ResponseEntity<?> loadMasters() {
		MSAResponse msaResponse = utilityFeignClient.loadMasters();
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

	/* (non-Javadoc)
	 * @see com.mindzen.planner.service.UtilityService#loadTimeZones()
	 */
	@Override
	public ResponseEntity<?> loadTimeZones() {
		MSAResponse msaResponse = utilityFeignClient.loadTimeZones();
		if(msaResponse.isSuccess()) {
			return new ResponseEntity<>(new ApiSuccessResponse(msaResponse), msaResponse.getHttpStatus());
		}
		else
			throw new CustomRuntimeException(msaResponse);
	}

}
