/**
 * 
 */
package com.mindzen.planner.config;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariDataSource;

import lombok.extern.slf4j.Slf4j;

/**
 * A factory for creating Connection objects.
 *
 * @author Alexpandiyan Chokkan
 * 
 * 12-Oct-2018
 */

/** The Constant log. */
@Slf4j
public class ConnectionFactory {

	/** The data source. */
	private static HikariDataSource dataSource;

	/**
	 * Instantiates a new connection factory.
	 */
	private ConnectionFactory() {
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	public static Connection getConnection() {
		String database="wenplan";
		String username="postgres";
		String password="postGreSql";
		Connection connection = null;

		synchronized(ConnectionFactory.class) {
			if(null == dataSource) {
				String host = "postgres";
				connection = getDataSourceConnection(host, database, username, password);
				if(null == connection) {
					host = "192.168.1.134";
					connection = getDataSourceConnection(host, database, username, password);
				}
				if(null == connection) {
					host = "localhost";
					connection = getDataSourceConnection(host, database, username, password);
				}
			}
			else
				connection = getNewDatabaseConnection(dataSource);

		}
		return connection;
	}

	/**
	 * Gets the data source connection.
	 *
	 * @param host the host
	 * @param database the database
	 * @param username the username
	 * @param password the password
	 * @return the data source connection
	 */
	private static Connection getDataSourceConnection(String host, String database, String username, String password) {
		dataSource = new HikariDataSource();
		dataSource.setJdbcUrl("jdbc:postgresql://"+host+":5432/"+database);
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		return getNewDatabaseConnection(dataSource);
	}

	/**
	 * Gets the new database connection.
	 *
	 * @param dataSource the data source
	 * @return the new database connection
	 */
	private static Connection getNewDatabaseConnection(HikariDataSource dataSource) {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			log.debug("Logger db connected successfully.");
		} catch (SQLException e) {
			log.error("Logger db connection failed. SQL ErrorCode: "+e.getSQLState()+".  Msg: "+e.getMessage());
		}
		return connection;
	}

}