/**
 * 
 */
package com.mindzen.planner.config;

import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.mindzen.infra.api.response.MSAResponse;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomResponseBodyAdvice.
 *
 * @author Alexpandiyan Chokkan
 */
@RestControllerAdvice
public class CustomResponseBodyAdvice implements ResponseBodyAdvice<Object> {

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice#supports(org.springframework.core.MethodParameter, java.lang.Class)
	 */

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice#beforeBodyWrite(java.lang.Object, org.springframework.core.MethodParameter, org.springframework.http.MediaType, java.lang.Class, org.springframework.http.server.ServerHttpRequest, org.springframework.http.server.ServerHttpResponse)
	 */
	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
					ServerHttpResponse response) {
		MultiValueMap<String, String> responses = new HttpHeaders();
		if(null != body) {
			if(body.getClass().equals(MSAResponse.class)) {
				MSAResponse msaResponse = (MSAResponse) body;
				Boolean success = (boolean) msaResponse.isSuccess();
				responses.add("responseStatus", success.toString());
				responses.add("responseMessage", (String)msaResponse.getMessage());
			}
		}else {
			responses.add("responseStatus", "Response body is null");
			responses.add("responseMessage", "No Response");
		}
		response.getHeaders().addAll(responses);
		return body;
	}

}