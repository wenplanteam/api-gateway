/**
 * 
 */ 
package com.mindzen.planner.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

// TODO: Auto-generated Javadoc
/**
 * The Class SwaggerConfig.
 *
 * @author Alexpandiyan Chokkan
 * 
 * 21-Sep-2018
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	/**
	 * Api method controls the endpoints exposed by Swagger.
	 *
	 * @return the docket
	 */
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.mindzen.planner.controller"))
				.paths(PathSelectors.ant("/**"))
				.build()
				.apiInfo(apiInfo())
				.useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.GET, responseMessages())
				.securitySchemes(Arrays.asList(new ApiKey("Bearer", "Authorization", "header")));
	}

	/**
	 * Api info.
	 *
	 * @return the api info
	 */
	private ApiInfo apiInfo() {
		return new ApiInfo(
				"Wenplan Rest API Gateway", 
				"", 
				"", 
				"", 
				new Contact("", "", ""), 
				"", 
				"", 
				Collections.emptyList());
	}

	/**
	 * Response messages.
	 *
	 * @return the list
	 */
	private  List<ResponseMessage> responseMessages() {
		List<ResponseMessage> responseMessages = new ArrayList<>();
		responseMessages.add(new ResponseMessageBuilder()
				.code(200)
				.message("Success")
				.responseModel(new ModelRef("Success"))
				.build());
		responseMessages.add(new ResponseMessageBuilder()
				.code(400)
				.message("Client Error: Bad Request")
				.build());
		responseMessages.add(new ResponseMessageBuilder()
				.code(401)
				.message("Client Error: Unauthorized")
				.build());
		responseMessages.add(new ResponseMessageBuilder()
				.code(403)
				.message("Client Error: Forbidden")
				.build());
		responseMessages.add(new ResponseMessageBuilder()
				.code(404)
				.message("Client Error: Not Found")
				.build());
		responseMessages.add(new ResponseMessageBuilder()
				.code(500)
				.message("Server Error: Internal Error")
				.build());

		return responseMessages;
	}

}
