
/**
 * 
 */
package com.mindzen.planner.config.feign;

import java.io.File;

import javax.validation.Valid;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.FeignConfiguration;
import com.mindzen.planner.dto.LookaheadDTO;
import com.mindzen.planner.dto.LookaheadListDTO;
import com.mindzen.planner.dto.ProgressCompletionRequestDTO;
import com.mindzen.planner.dto.ProjectCompanyDTO;
import com.mindzen.planner.dto.ProjectDTO;
import com.mindzen.planner.dto.ProjectUserDTO;
import com.mindzen.planner.dto.UserDTO;

import feign.Headers;

/**
 * The Interface UserServiceProxy.
 *
 * @author Bharath Telukuntla
 * @RibbonClient will be
 * used to load balance the backend requests
 * @FeignClient will be used as an abstraction over Rest-based calls, by which Microservice can communicate with each other
 */
@FeignClient(name = "project-management", configuration=FeignConfiguration.class)
@RibbonClient(name = "project-management")
@RequestMapping("/api/projectManagement")
public interface ProjectManagementFeignClient {

	/**
	 * Creates the project.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@PostMapping("/project")
	public MSAResponse createProject(@RequestBody ProjectDTO projectDTO);

	/**
	 * Share project to user.
	 *
	 * @param projectUserDTO the project user DTO
	 * @return the MSA response
	 */
	@PostMapping("/shareProject")
	public MSAResponse shareProjectToUser(@RequestBody ProjectUserDTO projectUserDTO);

	/**
	 * Update project.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@PutMapping("/project")
	public MSAResponse updateProject(@RequestBody ProjectDTO projectDTO);

	/**
	 * Delete project.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	@DeleteMapping("/project")
	public MSAResponse deleteProject(@RequestBody Long[] projectId);

	/**
	 * Load all projects.
	 *
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param status the status
	 * @param searchTerm the search term
	 * @param created the created
	 * @return the MSA response
	 */
	@GetMapping("/projects")
	public MSAResponse loadAllProjects(@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = false, value = "status") Boolean status,
			@RequestParam(required = false, value = "searchTerm") String searchTerm,
			@RequestParam(required = true, value = "created") Long created);

	/**
	 * Find by project id.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	@GetMapping("/project")
	public MSAResponse findByProjectId(@RequestParam(required = true, value = "projectId")Long projectId);

	/**
	 * Creates the lookahead.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the MSA response
	 */
	@PostMapping("/lookahead")
	public MSAResponse createLookahead(@RequestBody LookaheadDTO lookaheadDTO);

	/**
	 * List lookahead.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the MSA response
	 */
	//@PostMapping("/lookaheads") written by alex
	@PostMapping("/getLookaheads") //changes added by naveen for performace tunning  
	public MSAResponse listLookahead(@RequestBody LookaheadListDTO lookaheadListDTO);

	/**
	 * Find project company by project id.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	@GetMapping("/projectCompany")
	public MSAResponse findProjectCompanyByProjectId(@RequestParam(required = true,value = "projectId")Long projectId);

	/**
	 * Insert project companies.
	 *
	 * @param projectCompanyDTO the project company DTO
	 * @return the MSA response
	 */
	@PostMapping("/projectCompany")
	public MSAResponse insertProjectCompanies(@RequestBody ProjectCompanyDTO projectCompanyDTO);

	/**
	 * Find by project for plan sharing.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	@GetMapping("/projectPlanShare")
	public MSAResponse findByProjectForPlanSharing(@RequestParam(required = true ,value = "projectId")Long projectId);

	/**
	 * Lookahead info.
	 *
	 * @param projectId the project id
	 * @return the MSA response
	 */
	@GetMapping("/lookaheadDateInfo")
	public MSAResponse lookaheadInfo(@RequestParam(required=true, value="projectId") Long projectId);

	/**
	 * Update lookahead.
	 *
	 * @param lookaheadDTO the lookahead DTO
	 * @return the MSA response
	 */
	@PutMapping("/lookahead")
	public MSAResponse updateLookahead(@RequestBody LookaheadDTO lookaheadDTO);

	/**
	 * Lookahead by id.
	 *
	 * @param taskId the task id
	 * @param subTaskId the sub task id
	 * @return the MSA response
	 */
	@DeleteMapping("lookaheadById")
	public MSAResponse lookaheadById(
			@RequestParam(required = false, value = "taskId") Long taskId,
			@RequestParam(required = false, value = "subTaskId") Long subTaskId);

	/**
	 * Update project shared user details.
	 *
	 * @param userDTO the user DTO
	 * @return the MSA response
	 */
	@PostMapping("/updateProjectSharedUserDetails")
	public MSAResponse updateProjectSharedUserDetails(@RequestBody UserDTO userDTO);

	/**
	 * Lookahead PDF report.
	 *
	 * @param lookaheadListDTO the lookahead list DTO
	 * @return the MSA response
	 */
	// @PostMapping("lookaheadPDFReport")  changes added by Naveen for lookahead report (downloadPdF method) In project MSA
	@PostMapping("/downloadPDF")
	public MSAResponse lookaheadPDFReport(@RequestBody LookaheadListDTO lookaheadListDTO);

	/**
	 * Gets the project info.
	 *
	 * @param projectId the project id
	 * @return the project info
	 */
	@GetMapping("/projectInfo")
	public MSAResponse getProjectInfo(@RequestParam(required = true, value = "projectId") Long projectId);

	/**
	 * Insert project info.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@PostMapping("/projectInfo")
	public MSAResponse insertProjectInfo(@RequestBody ProjectDTO projectDTO);

	/**
	 * User company associate to project.
	 *
	 * @param userId the user id
	 * @return the MSA response
	 */
	@GetMapping("/userCompanyAssociateToProject")
	public MSAResponse userCompanyAssociateToProject(
			@RequestParam(required = true, value = "userId") Long userId);

	/**
	 * Insert trade.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@PostMapping("/trade")
	public MSAResponse insertTrade(@RequestBody ProjectDTO projectDTO);

	/**
	 * Gets the trade.
	 *
	 * @param projectId the project id
	 * @return the trade
	 */
	@GetMapping("/trade")
	public MSAResponse getTrade(@RequestParam(required = true, value = "projectId") Long projectId);

	/**
	 * Gets the GC by user.
	 *
	 * @return the GC by user
	 */
	@GetMapping("/gcByUser")
	public MSAResponse getGCByUser();

	/**
	 * Update project companies.
	 *
	 * @param userDTO the user DTO
	 * @return the MSA response
	 */
	@PutMapping("/projectCompany")
	public MSAResponse updateProjectCompanies(@RequestBody UserDTO userDTO);
	
	/**
	 * Gets the task summary mail.
	 *
	 * @param projectId the project id
	 * @return the task summary mail
	 */
	@GetMapping("/taskSummaryMail/{projectId}")
	public MSAResponse getTaskSummaryMail(@PathVariable(required =true, value= "projectId") Long projectId);

	/**
	 * Gets the spec division.
	 *
	 * @param projectId the project id
	 * @return the spec division
	 */
	@GetMapping("/specDivision")
	public MSAResponse getSpecDivision(@RequestParam(required = true, value = "projectId") Long projectId);

	/**
	 * Insert spec division.
	 *
	 * @param projectDTO the project DTO
	 * @return the MSA response
	 */
	@PostMapping("/specDivision")
	public MSAResponse insertSpecDivision(ProjectDTO projectDTO);
	
	/**
	 * Gets the progress completion by project id.
	 *
	 * @param projectCompletionRequestDTO the project completion request DTO
	 * @return the progress completion by project id
	 */
	@PostMapping("/progressCompletionByProjectId")
	public MSAResponse getProgressCompletionByProjectId(
			@Valid ProgressCompletionRequestDTO projectCompletionRequestDTO);
	
	@PostMapping("/downloadPercentageCompletePDF")
	public MSAResponse downloadPercentageCompletePDF(@RequestBody ProgressCompletionRequestDTO progressCompletionRequestDTO);

	/**
	 * Upload excel to lookahead.
	 *
	 * @param fileName the file name
	 * @param projectId the project id
	 * @param projectCompanyId the project company id
	 * @return the MSA response
	 */
	@PostMapping("/uploadLookaheadExcelFileName")
	public MSAResponse uploadLookaheadExcel(@RequestParam(required = true, value = "fileName") String fileName,@RequestParam(required = true, value = "projectId") Long projectId,@RequestParam(required = true, value = "projectCompanyId") Long projectCompanyId);

	@PostMapping("/importExcel")
	@Headers("Content-Type: multipart/form-data")
	public MSAResponse importExcel(@RequestParam(required = true, value = "excelFileName") String excelFileName,
			@RequestParam(required = true, value = "projectId") Long projectId,@RequestParam(required = true, value = "projectCompanyId") Long projectCompanyId) throws Exception;

	@PostMapping("/uploadFile1")
    public MSAResponse uploadFile(@RequestParam(required = true, value = "file") File file);

	@PostMapping("/importMppFile")
	public MSAResponse importMppFile(@RequestParam(required = true, value = "mppFileName") String mppFileName,
			@RequestParam(required = true, value = "projectId") Long projectId,@RequestParam(required = true, value = "projectCompanyId") Long projectCompanyId) throws Exception;
	
	@PutMapping("/updateMppFile")
	public MSAResponse updateMppFile(@RequestParam(required = true, value = "mppFileName") String mppFileName,
			@RequestParam(required = true, value = "projectId") Long projectId,@RequestParam(required = true, value = "projectCompanyId") Long projectCompanyId, @RequestParam(required = true,value="importHistoryId" )Long importHistoryId) throws Exception;

	@PostMapping("/importXerFile")
	public MSAResponse importXerFile(@RequestParam(required = true, value = "xerFileName") String xerFileName,
			@RequestParam(required = true, value = "projectId") Long projectId,@RequestParam(required = true, value = "projectCompanyId") Long projectCompanyId) throws Exception;

	@GetMapping("/importHistory")
	public MSAResponse importHistory(@RequestParam(required = true, value = "projectId") Long projectId, @RequestParam (required = true ,value = "pageNo" )Integer pageNo, @RequestParam (required = true,value ="pageSize") Integer pageSize);

	@PutMapping("/clearSchedule")
	public MSAResponse clearSchedule(@RequestParam(required = true, value = "projectId") Long projectId,@RequestParam(required = true, value = "projectCompanyId") Long projectCompanyId, @RequestParam(required = true,value="importHistoryId" )Long importHistoryId) throws Exception;
}

