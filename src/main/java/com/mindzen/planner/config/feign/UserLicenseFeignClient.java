/**
 * 
 */
package com.mindzen.planner.config.feign;

import java.util.List;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.FeignConfiguration;
import com.mindzen.planner.dto.AuthenticationRequest;
import com.mindzen.planner.dto.CompanyDTO;
import com.mindzen.planner.dto.LicenseDistributionDTO;
import com.mindzen.planner.dto.PlanDTO;
import com.mindzen.planner.dto.StripeDataDTO;
import com.mindzen.planner.dto.UserDTO;
import com.mindzen.planner.dto.UserPlanMasterDTO;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserLicenseFeignClient.
 *
 * @author Alexpandiyan Chokkan
 * 
 * 21-Sep-2018
 * @RibbonClient is a LoadBalancer the discovery clients
 * @FeignClient will be used as an abstraction over Rest-based calls, by which Microservice can communicate with each other
 */
@FeignClient(name = "user-license", configuration=FeignConfiguration.class)
@RibbonClient(name = "user-license")
@RequestMapping("/api/userLicense")
public interface UserLicenseFeignClient {

	/**
	 * Find by email.
	 *
	 * @param email the email
	 * @return the user DTO
	 */
	@GetMapping("/findUserByEmail")
	public MSAResponse findByEmail(@RequestParam("email") String email);

	/**
	 * User creation.
	 *
	 * @param userDTO the user DTO
	 * @return the MSA response
	 */
	@PostMapping("/user")
	public MSAResponse createUser(@RequestBody UserDTO userDTO);

	/**
	 * create User By Basic Info.
	 *
	 * @param userDTOs the user DT os
	 * @return the MSA response
	 */
	@PostMapping("/createUserByBasicInfo")
	public MSAResponse createUserByBasicInfo(@RequestBody List<UserDTO> userDTOs);

	/**
	 * verify Email.
	 *
	 * @param email the email
	 * @param source the source
	 * @return the MSA response
	 */

	@GetMapping("/verifyEmail")
	public MSAResponse verifyEmail(@RequestParam(required = true, value = "email") String email
			,@RequestParam(required = true,value = "source") String source);

	/**
	 * loadUsers.
	 *
	 * @param userId the user id
	 * @param createdBy the created by
	 * @param projectId the project id
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param created the created
	 * @return the MSA response
	 */
	@GetMapping("/users")
	public MSAResponse loadUsers(
			@RequestParam(required = false, value = "userId") Long userId,
			@RequestParam(required = false ,value = "createdBy") Boolean createdBy,
			@RequestParam(required = false ,value = "projectId") Long projectId,
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = true, value = "created") Long created);

	/**
	 * updateUser.
	 *
	 * @param userDTO the user DTO
	 * @return the MSA response
	 */
	@PutMapping("user")
	public MSAResponse updateUser(@RequestBody UserDTO userDTO); 

	/**
	 * deleteUser.
	 *
	 * @param userIds the user ids
	 * @return the MSA response
	 */
	@DeleteMapping("/user")
	public MSAResponse deleteUser(@RequestBody List<Long> userIds);

	/**
	 * decode.
	 *
	 * @param data the data
	 * @return the MSA response
	 */
	@GetMapping("/decode")
	public MSAResponse decode(String data);

	/**
	 * List of users.
	 *
	 * @param userDTOs the user DT os
	 * @return msaresponse
	 */
	@PostMapping("/projectUsers")
	public MSAResponse listOfUsers(List<UserDTO> userDTOs);

	/**
	 * Find project user by project id.
	 *
	 * @param projectId the project id
	 * @return msa Response
	 */
	@GetMapping("/projectUsers")
	public MSAResponse findProjectUserByProjectId(@RequestParam(required = true,value = "projectId") Long projectId );

	/**
	 * Delete user by email.
	 *
	 * @param email the email
	 * @return the MSA response
	 */
	@DeleteMapping("/delete")
	public MSAResponse deleteUserByEmail(@RequestParam(required = true ,value = "email")String email); 

	/**
	 * Find users by ids.
	 *
	 * @param usersIds the users ids
	 * @return the MSA response
	 */
	@PostMapping("/findUsersByIds")
	public MSAResponse findUsersByIds(@RequestBody List<Long> usersIds);

	/**
	 * Find license distribution id.
	 *
	 * @return the MSA response
	 */
	@GetMapping("/findLicenseDistributionId")
	public MSAResponse findLicenseDistributionId();

	/**
	 * Change password.
	 *
	 * @param authenticationRequest the authentication request
	 * @return the MSA response
	 */
	@PostMapping("/changePassword")
	public MSAResponse changePassword(@RequestBody AuthenticationRequest authenticationRequest);

	/**
	 * Forgot password.
	 *
	 * @param authenticationRequest the authentication request
	 * @return the MSA response
	 */
	@PostMapping("/forgotPassword")
	public MSAResponse forgotPassword(@RequestBody AuthenticationRequest authenticationRequest);

	/**
	 * Find company by user id.
	 *
	 * @return the MSA response
	 */
	@GetMapping("findCompanyByUserId")
	public MSAResponse findCompanyByUserId();

	/**
	 * Company list.
	 *
	 * @param id the id
	 * @param createdBy the created by
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the MSA response
	 */
	@GetMapping("/company")
	public MSAResponse companyList(@RequestParam(required = false ,value = "id") Long id,
			@RequestParam(required = false ,value = "createdBy") Boolean createdBy,
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize);

	/**
	 * Insert company.
	 *
	 * @param companyDTO the company DTO
	 * @return the MSA response
	 */
	@PostMapping("/company")
	public MSAResponse insertCompany(@RequestBody CompanyDTO companyDTO);

	/**
	 * Update company.
	 *
	 * @param companyDTO the company DTO
	 * @return the MSA response
	 */
	@PutMapping("/company")
	public MSAResponse updateCompany(@RequestBody CompanyDTO companyDTO);

	/**
	 * In activate company.
	 *
	 * @param companyId the company id
	 * @return the MSA response
	 */
	@DeleteMapping("/company")
	public MSAResponse inActivateCompany(@RequestBody List<Long> companyId);

	/*      
	 *  
	 *  License services
	 *  
	 */

	/**
	 * Load plans.
	 *
	 * @param paid the paid
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param id the id
	 * @return the MSA response
	 */
	@GetMapping("/plans")
	public MSAResponse loadPlans(@RequestParam(required = false ,value = "paid") boolean paid,
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = false ,value = "id") Long id);
	
	/**
	 * Creates the plan.
	 *
	 * @param planDTO the plan DTO
	 * @return the MSA response
	 */
	@PostMapping("/plan")
	public MSAResponse createPlan(@RequestBody PlanDTO planDTO);
	
	/**
	 * Update plan.
	 *
	 * @param planDTO the plan DTO
	 * @return the MSA response
	 */
	@PutMapping("/plan")
	public MSAResponse updatePlan(@RequestBody PlanDTO planDTO);
	
	/**
	 * Delete plan.
	 *
	 * @param planId the plan id
	 * @return the MSA response
	 */
	@DeleteMapping("/plan")
	public MSAResponse deletePlan(@RequestBody List<Long> planId);

	/**
	 * User licenses.
	 *
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @param status the status
	 * @return the MSA response
	 */
	@GetMapping("/licenses")
	public MSAResponse userLicenses(@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize,
			@RequestParam(required = false, value = "status") String status);

	/**
	 * Buy license.
	 *
	 * @param userPlanMasterDTO the user plan master DTO
	 * @return the MSA response
	 */
	@PostMapping("/buyLicense")
	public MSAResponse buyLicense(@RequestBody UserPlanMasterDTO userPlanMasterDTO);

	/**
	 * User license count.
	 *
	 * @return the MSA response
	 */
	@GetMapping("licenseCount")
	public MSAResponse userLicenseCount();
	
	/**
	 * Provide license.
	 *
	 * @param licenseDistributionDTO the license distribution DTO
	 * @return the MSA response
	 */
	@PostMapping("/provideLicense")
	public MSAResponse provideLicense(@RequestBody LicenseDistributionDTO licenseDistributionDTO);
	
	/**
	 * Revoke license.
	 *
	 * @param licenseDistributionDTO the license distribution DTO
	 * @return the MSA response
	 */
	@PostMapping("/revokeLicense")
	public MSAResponse revokeLicense(@RequestBody LicenseDistributionDTO licenseDistributionDTO);
	
	/**
	 * User license availability.
	 *
	 * @param email the email
	 * @return the MSA response
	 */
	@GetMapping("/userLicenseAvailability")
	public MSAResponse userLicenseAvailability(@RequestParam(required = true, value = "email") String email);
	
	/**
	 * Creates the basic user.
	 *
	 * @param userDTO the user DTO
	 * @return the MSA response
	 */
	@PostMapping("/createBasicUser")
	public MSAResponse createBasicUser(@RequestBody UserDTO userDTO);
	
	/**
	 * Find users by email.
	 *
	 * @param email the email
	 * @return the MSA response
	 */
	@GetMapping("/findUsersByEmail")
	public MSAResponse findUsersByEmail(@RequestParam(required=true, value = "email") String email);
	
	/**
	 * Gets the license response.
	 *
	 * @param stripeData the stripe data
	 * @return the license response
	 */
	@PostMapping("/charge")
	public MSAResponse getLicenseResponse(@RequestBody StripeDataDTO stripeData);
	
	/**
	 * Find expirydate.
	 *
	 * @param billingCycle the billing cycle
	 * @return the MSA response
	 */
	@GetMapping("/expirydate")
	public MSAResponse findExpirydate(@RequestParam(required = false, value = "billingCycle") String billingCycle);

	/**
	 * Gets the all user informations.
	 *
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the all user informations
	 */
	@GetMapping("/userDetails")
	public MSAResponse getAllUserInformations(
			@RequestParam(required = false, value = "pageNo") Integer pageNo,
			@RequestParam(required = false, value = "pageSize") Integer pageSize);
}

