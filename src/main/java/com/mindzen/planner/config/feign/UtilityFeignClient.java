/**
 * 
 */
package com.mindzen.planner.config.feign;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mindzen.infra.api.response.MSAResponse;
import com.mindzen.planner.config.FeignConfiguration;
import com.mindzen.planner.dto.LookaheadJasperReportRootDTO;
import com.mindzen.planner.dto.PercentCompleteJasperReportDetailRootDTO;


/**
 * The Interface UtilityFeignClient.
 * @RibbonClient will be
 * used to load balance the backend requests
 * 
 * @FeignClient will be used as an abstraction over Rest-based calls, by which Microservice can communicate with each other
 * 
 *    
 */
@FeignClient(name = "utility", configuration=FeignConfiguration.class)
@RibbonClient(name = "utility")
@RequestMapping("/api/utility")
public interface UtilityFeignClient {

	@GetMapping("/masters")
	public MSAResponse loadMasters();
	
	@PostMapping("/pdfGeneration")
	public MSAResponse pdfGeneration(
			@RequestBody LookaheadJasperReportRootDTO lookaheadJasperReportRootDTO);

	@GetMapping("/timeZones")
	public MSAResponse loadTimeZones();

	@PostMapping("/percentCompletePDFGenerate")
	public MSAResponse generatePercentageCompleteReport(@RequestBody PercentCompleteJasperReportDetailRootDTO 
			percentCompleteJasperReportDetailRootDTO);
}

