package com.mindzen.planner.config;

import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.mindzen.infra.exception.CustomRuntimeException;
import com.mindzen.planner.security.factory.CustomUserDetails;

import feign.Contract;
import feign.Logger;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class FeignConfiguration {

	@Bean
	public Contract contract() {
		return new SpringMvcContract();
	}
	
	@Bean
	public Logger.Level level() {
		return Logger.Level.HEADERS;
	}
	
/*	@Bean
	public Encoder encoder() {
		return new CustomEncoder();
	}
*/		
	@Bean
	public RequestInterceptor requestInterceptor() {
		return new RequestInterceptor() {
			
			@Override
			public void apply(RequestTemplate template) {
				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
				Long userId;
				if(null == authentication || !authentication.isAuthenticated() || "anonymousUser".equals(authentication.getPrincipal()))
					userId = 1L;
				else
					userId = ((CustomUserDetails) authentication.getPrincipal()).getId();
				log.debug("userId: "+ userId);
				if(null == userId) {
					log.error("Userd id is not available");
					throw new CustomRuntimeException("Please contact admin", HttpStatus.INTERNAL_SERVER_ERROR);
				}
				template.header("userId", userId.toString());
			}
		};
	}
	
}
