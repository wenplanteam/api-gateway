/*
package com.mindzen.planner.config.audit;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.mindzen.planner.security.factory.CustomUserDetails;

*//**
 * The Class AuditorAwareImpl.
 *
 * @author Alexpandiyan Chokkan
 *//*
@Component
public class AuditorAwareImpl implements AuditorAware<Long> {

	 (non-Javadoc)
	 * @see org.springframework.data.domain.AuditorAware#getCurrentAuditor()

	@Override
	public Optional<Long> getCurrentAuditor() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(null == authentication || !authentication.isAuthenticated())
			return Optional.of(1L);
		
		CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
		
		return Optional.ofNullable(customUserDetails.getId());
	}

}
*/