package com.mindzen.planner.config;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.TeeOutputStream;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.mindzen.planner.security.factory.CustomUserDetails;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class ServiceLogFilter.
 * implemented to validate requests from UI.
 * ex .methods,bodyparameters and vice versa .
 *
 */
@Component
@Slf4j
public class ServiceLogFilter implements Filter {

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;

			Map<String, String> requestMap = this.getTypesafeRequestMap(httpServletRequest);
			BufferedRequestWrapper bufferedReqest = new BufferedRequestWrapper(httpServletRequest);
			BufferedResponseWrapper bufferedResponse = new BufferedResponseWrapper(httpServletResponse);
			final StringBuilder logMessage = new StringBuilder();
			logMessage.append("$USERID$");
			logMessage.append(" || $EMAIL$");
			logMessage.append(" || "+httpServletRequest.getMethod());
			//logMessage.append(" || "+httpServletRequest.getPathInfo());
			logMessage.append(" || "+httpServletRequest.getRequestURI());
			logMessage.append(" || "+requestMap); // REQUEST PARAMETERS
			logMessage.append(" || "+bufferedReqest.getRequestBody());
			String ipAddress = httpServletRequest.getHeader("x-forwarded-for");

			if (ipAddress == null) {
				ipAddress = request.getRemoteAddr();
			}
			logMessage.append(" || "+ipAddress);
			try {
				chain.doFilter(bufferedReqest, bufferedResponse);
				logMessage.append(" || "+bufferedResponse.getContent());
				LocalDateTime startTime = (LocalDateTime)request.getAttribute("startTime");
				LocalDateTime endTime = (LocalDateTime)request.getAttribute("endTime");
				logMessage.append(" || "+Duration.between(startTime, endTime).toMillis());

			} catch (Exception e) {
				e.getMessage();
				log.info("Response Issue :"+e.getMessage());
			}

			if(logMessage.toString().contains("swagger") || logMessage.toString().contains("font-") || 
					logMessage.toString().contains("PNG") || logMessage.toString().contains("wOF2") ) {
				logMessage.setLength(0);
				logMessage.append("Aceess Swagger UI");
			}
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			if(null == authentication || !authentication.isAuthenticated() || "anonymousUser".equals(authentication.getPrincipal())) {
				logMessage.replace(0, 20, 1+" || null");
			}
			else {
				CustomUserDetails userDetails =((CustomUserDetails) authentication.getPrincipal());
				logMessage.replace(0, 20, userDetails.getId()+" || "+userDetails.getUsername());
			}
			log.info(logMessage.toString());
		} catch (Exception e) {
			log.info(" service value issue :"+e.getMessage());
		}


	}

	/**
	 * Gets the typesafe request map.
	 *
	 * @param request
	 *            the request
	 * @return the typesafe request map
	 */
	private Map<String, String> getTypesafeRequestMap(HttpServletRequest request) {
		Map<String, String> typesafeRequestMap = new HashMap<String, String>();
		Enumeration<?> requestParamNames = request.getParameterNames();
		while (requestParamNames.hasMoreElements()) {
			String requestParamName = (String) requestParamNames.nextElement();
			String requestParamValue = request.getParameter(requestParamName);
			typesafeRequestMap.put(requestParamName, requestParamValue);
		}
		return typesafeRequestMap;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

	/**
	 * The Class BufferedRequestWrapper.
	 */
	private static final class BufferedRequestWrapper extends HttpServletRequestWrapper {

		/** The bais. */
		private ByteArrayInputStream bais = null;

		/** The baos. */
		private ByteArrayOutputStream baos = null;

		/** The bsis. */
		private BufferedServletInputStream bsis = null;

		/** The buffer. */
		private byte[] buffer = null;

		/**
		 * Instantiates a new buffered request wrapper.
		 *
		 * @param req
		 *            the req
		 * @throws IOException
		 *             Signals that an I/O exception has occurred.
		 */
		public BufferedRequestWrapper(HttpServletRequest req) throws IOException {
			super(req);
			// Read InputStream and store its content in a buffer.
			InputStream is = req.getInputStream();
			this.baos = new ByteArrayOutputStream();
			byte buf[] = new byte[1024];
			int letti;
			while ((letti = is.read(buf)) > 0) {
				this.baos.write(buf, 0, letti);
			}
			this.buffer = this.baos.toByteArray();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletRequestWrapper#getInputStream()
		 */
		@Override
		public ServletInputStream getInputStream() {
			this.bais = new ByteArrayInputStream(this.buffer);
			this.bsis = new BufferedServletInputStream(this.bais);
			return this.bsis;
		}

		/**
		 * Gets the request body.
		 *
		 * @return the request body
		 * @throws IOException
		 *             Signals that an I/O exception has occurred.
		 */
		String getRequestBody() throws IOException {
			BufferedReader reader = new BufferedReader(new InputStreamReader(this.getInputStream()));
			String line = null;
			StringBuilder inputBuffer = new StringBuilder();
			do {
				line = reader.readLine();
				if (null != line) {
					inputBuffer.append(line.trim());
				}
			} while (line != null);
			reader.close();
			return inputBuffer.toString().trim();
		}

	}

	/**
	 * The Class BufferedServletInputStream.
	 */
	private static final class BufferedServletInputStream extends ServletInputStream {

		/** The bais. */
		private ByteArrayInputStream bais;

		/**
		 * Instantiates a new buffered servlet input stream.
		 *
		 * @param bais
		 *            the bais
		 */
		public BufferedServletInputStream(ByteArrayInputStream bais) {
			this.bais = bais;
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.io.InputStream#available()
		 */
		@Override
		public int available() {
			return this.bais.available();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.io.InputStream#read()
		 */
		@Override
		public int read() {
			return this.bais.read();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.io.InputStream#read(byte[], int, int)
		 */
		@Override
		public int read(byte[] buf, int off, int len) {
			return this.bais.read(buf, off, len);
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletInputStream#isFinished()
		 */
		@Override
		public boolean isFinished() {
			// TODO Auto-generated method stub
			return false;
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletInputStream#isReady()
		 */
		@Override
		public boolean isReady() {
			// TODO Auto-generated method stub
			return false;
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletInputStream#setReadListener(javax.servlet.ReadListener)
		 */
		@Override
		public void setReadListener(ReadListener listener) {
			// TODO Auto-generated method stub

		}

	}

	/**
	 * The Class TeeServletOutputStream.
	 */
	public class TeeServletOutputStream extends ServletOutputStream {

		/** The target stream. */
		private final TeeOutputStream targetStream;

		/**
		 * Instantiates a new tee servlet output stream.
		 *
		 * @param one
		 *            the one
		 * @param two
		 *            the two
		 */
		public TeeServletOutputStream(OutputStream one, OutputStream two) {
			targetStream = new TeeOutputStream(one, two);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.io.OutputStream#write(int)
		 */
		@Override
		public void write(int arg0) throws IOException {
			this.targetStream.write(arg0);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.io.OutputStream#flush()
		 */
		public void flush() throws IOException {
			super.flush();
			this.targetStream.flush();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see java.io.OutputStream#close()
		 */
		public void close() throws IOException {
			super.close();
			this.targetStream.close();
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletOutputStream#isReady()
		 */
		@Override
		public boolean isReady() {
			return false;
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletOutputStream#setWriteListener(javax.servlet.WriteListener)
		 */
		@Override
		public void setWriteListener(WriteListener listener) {

		}
	}

	/**
	 * The Class BufferedResponseWrapper.
	 */
	public class BufferedResponseWrapper implements HttpServletResponse {

		/** The original. */
		HttpServletResponse original;

		/** The tee. */
		TeeServletOutputStream tee;

		/** The bos. */
		ByteArrayOutputStream bos;

		/**
		 * Instantiates a new buffered response wrapper.
		 *
		 * @param response
		 *            the response
		 */
		public BufferedResponseWrapper(HttpServletResponse response) {
			original = response;
		}

		/**
		 * Gets the content.
		 *
		 * @return the content
		 */
		public String getContent() {
			return bos.toString();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#getWriter()
		 */
		public PrintWriter getWriter() throws IOException {
			return original.getWriter();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#getOutputStream()
		 */
		public ServletOutputStream getOutputStream() throws IOException {
			if (tee == null) {
				bos = new ByteArrayOutputStream();
				tee = new TeeServletOutputStream(original.getOutputStream(), bos);
			}
			return tee;

		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#getCharacterEncoding()
		 */
		@Override
		public String getCharacterEncoding() {
			return original.getCharacterEncoding();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#getContentType()
		 */
		@Override
		public String getContentType() {
			return original.getContentType();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.servlet.ServletResponse#setCharacterEncoding(java.lang.String)
		 */
		@Override
		public void setCharacterEncoding(String charset) {
			original.setCharacterEncoding(charset);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#setContentLength(int)
		 */
		@Override
		public void setContentLength(int len) {
			original.setContentLength(len);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#setContentType(java.lang.String)
		 */
		@Override
		public void setContentType(String type) {
			original.setContentType(type);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#setBufferSize(int)
		 */
		@Override
		public void setBufferSize(int size) {
			original.setBufferSize(size);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#getBufferSize()
		 */
		@Override
		public int getBufferSize() {
			return original.getBufferSize();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#flushBuffer()
		 */
		@Override
		public void flushBuffer() throws IOException {
			tee.flush();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#resetBuffer()
		 */
		@Override
		public void resetBuffer() {
			original.resetBuffer();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#isCommitted()
		 */
		@Override
		public boolean isCommitted() {
			return original.isCommitted();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#reset()
		 */
		@Override
		public void reset() {
			original.reset();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#setLocale(java.util.Locale)
		 */
		@Override
		public void setLocale(Locale loc) {
			original.setLocale(loc);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.ServletResponse#getLocale()
		 */
		@Override
		public Locale getLocale() {
			return original.getLocale();
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.servlet.http.HttpServletResponse#addCookie(javax.servlet.http.
		 * Cookie)
		 */
		@Override
		public void addCookie(Cookie cookie) {

			original.addCookie(cookie);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.http.HttpServletResponse#containsHeader(java.lang.
		 * String)
		 */
		@Override
		public boolean containsHeader(String name) {
			return original.containsHeader(name);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.servlet.http.HttpServletResponse#encodeURL(java.lang.String)
		 */
		@Override
		public String encodeURL(String url) {
			return original.encodeURL(url);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.servlet.http.HttpServletResponse#encodeRedirectURL(java.lang.
		 * String)
		 */
		@Override
		public String encodeRedirectURL(String url) {
			return original.encodeRedirectURL(url);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.servlet.http.HttpServletResponse#encodeUrl(java.lang.String)
		 */
		@SuppressWarnings("deprecation")
		@Override
		public String encodeUrl(String url) {
			return original.encodeUrl(url);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.servlet.http.HttpServletResponse#encodeRedirectUrl(java.lang.
		 * String)
		 */
		@SuppressWarnings("deprecation")
		@Override
		public String encodeRedirectUrl(String url) {
			return original.encodeRedirectUrl(url);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.http.HttpServletResponse#sendError(int,
		 * java.lang.String)
		 */
		@Override
		public void sendError(int sc, String msg) throws IOException {
			original.sendError(sc, msg);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.http.HttpServletResponse#sendError(int)
		 */
		@Override
		public void sendError(int sc) throws IOException {
			original.sendError(sc);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.servlet.http.HttpServletResponse#sendRedirect(java.lang.String)
		 */
		@Override
		public void sendRedirect(String location) throws IOException {
			original.sendRedirect(location);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.http.HttpServletResponse#setDateHeader(java.lang.
		 * String, long)
		 */
		@Override
		public void setDateHeader(String name, long date) {
			original.setDateHeader(name, date);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.http.HttpServletResponse#addDateHeader(java.lang.
		 * String, long)
		 */
		@Override
		public void addDateHeader(String name, long date) {
			original.addDateHeader(name, date);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.servlet.http.HttpServletResponse#setHeader(java.lang.String,
		 * java.lang.String)
		 */
		@Override
		public void setHeader(String name, String value) {
			original.setHeader(name, value);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.servlet.http.HttpServletResponse#addHeader(java.lang.String,
		 * java.lang.String)
		 */
		@Override
		public void addHeader(String name, String value) {
			original.addHeader(name, value);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.servlet.http.HttpServletResponse#setIntHeader(java.lang.String,
		 * int)
		 */
		@Override
		public void setIntHeader(String name, int value) {
			original.setIntHeader(name, value);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see
		 * javax.servlet.http.HttpServletResponse#addIntHeader(java.lang.String,
		 * int)
		 */
		@Override
		public void addIntHeader(String name, int value) {
			original.addIntHeader(name, value);
		}

		/** The Httpstatus. */
		int Httpstatus = 0;

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.http.HttpServletResponse#setStatus(int)
		 */
		@Override
		public void setStatus(int sc) {

			Httpstatus = sc;
			original.setStatus(sc);
		}

		/**
		 * Gets the status.
		 *
		 * @return the status
		 */
		public int getStatus() {

			return Httpstatus;
		}

		/**
		 * Gets the status I.
		 *
		 * @return the status I
		 */
		public int getStatusI() {

			return Httpstatus;
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see javax.servlet.http.HttpServletResponse#setStatus(int,
		 * java.lang.String)
		 */
		@SuppressWarnings("deprecation")
		@Override
		public void setStatus(int sc, String sm) {
			original.setStatus(sc, sm);
		}

		/* (non-Javadoc)
		 * @see javax.servlet.ServletResponse#setContentLengthLong(long)
		 */
		@Override
		public void setContentLengthLong(long length) {

		}

		/* (non-Javadoc)
		 * @see javax.servlet.http.HttpServletResponse#getHeader(java.lang.String)
		 */
		@Override
		public String getHeader(String name) {
			return null;
		}

		/* (non-Javadoc)
		 * @see javax.servlet.http.HttpServletResponse#getHeaders(java.lang.String)
		 */
		@Override
		public Collection<String> getHeaders(String name) {
			return null;
		}

		/* (non-Javadoc)
		 * @see javax.servlet.http.HttpServletResponse#getHeaderNames()
		 */
		@Override
		public Collection<String> getHeaderNames() {
			return null;
		}

	}

}