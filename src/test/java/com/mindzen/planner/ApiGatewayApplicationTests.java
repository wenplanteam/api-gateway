package com.mindzen.planner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

// TODO: Auto-generated Javadoc
/**
 * The Class ApiGatewayApplicationTests.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiGatewayApplicationTests {

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

}
