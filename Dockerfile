FROM openjdk:8
COPY src/main/resources/application.yml /root/.m2/repository/com/mindzen/infra/
COPY src/main/resources/application.yml $HOME/
COPY src/main/resources/RWOperations.txt /home/mindzen/RWOperations.java
COPY plugin/ /root/.m2/repository/com/mindzen/
COPY src/main/resources/trigger.sh /home/mindzen/trigger.sh
COPY src/main/resources/wenplan.xlsx /home/mindzen/wenplan.xlsx
COPY build/libs/api-gateway-0.0.1-SNAPSHOT.jar api-gateway-0.0.1-SNAPSHOT.jar
EXPOSE 9900
ENTRYPOINT ["java" , "-jar", "api-gateway-0.0.1-SNAPSHOT.jar"]